//
//  AddSchedual.swift
//  Asthma
//
//  Created by MOHAMED on 6/21/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit

class AddSchedual: UIViewController {

    @IBOutlet weak var LocationText: UITextField!
    @IBOutlet weak var UserNameText: UILabel!
    
    @IBOutlet weak var LastLoginText: UILabel!
    @IBOutlet weak var NameText: UITextField!
    
    @IBOutlet weak var UserImage: UIButton!
    
    var startDate : String!
    var endDate : String!
    var repeatDate : String!
    var alertTime : String!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.GetMedicalRepData(api_token: api_token){ (error , last_login , name , photo) in
                if error == nil {
                    self.LastLoginText.text = last_login
                    self.UserNameText.text = name
                    if photo != nil {
                        let ImageUrl = URL(string: ImageRoot + photo!)!
                        self.UserImage.kf.setImage(with: ImageUrl, for: .normal)
                    }else {
                        self.UserImage.setImage(#imageLiteral(resourceName: "Avatar"), for: .normal)
                    }                }
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func MenuBuAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuVC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    @IBAction func NotificationAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SchedualScreen") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func LogoutAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutScreen") as! LogoutVC
        self.present(vc, animated: true, completion: nil)
    }

    
    @IBAction func StartBu(_ sender: UIButton) {
        self.showDatesPickerViewController { date in
           
            
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            self.startDate = formatter.string(from: date)
            print(self.startDate)
        }

    }
    @IBAction func EndBu(_ sender: UIButton) {
        self.showDatesPickerViewController { date in
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            self.endDate = formatter.string(from: date)
            print(self.endDate)
        }

    }

    @IBAction func RepeatBu(_ sender: UIButton) {
        self.showDatesPickerViewController { date in
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
           
            self.repeatDate = formatter.string(from: date)
           // sender.setTitle(date.readableDate, for: .normal)
        }
        /*
        self.showDatesPickerViewController { date in
         
            let customDate = date
            print("Custom Date" , customDate)
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
           // sender.setTitle(<#T##title: String?##String?#>, for: <#T##UIControlState#>)
          //  self.repeatDate = formatter.string(from: customDate)
            self.repeatDate = date
            print(self.repeatDate)
        }

        */
    }
    
    @IBAction func AlertBu(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        let one = "30 Minutes"
        let two = "1 Hour"
        let three = "2 Hours"
        
        alert.addAction(UIAlertAction.init(title: one, style: .default, handler: { (action) in
            self.alertTime = one
        }))
        
        alert.addAction(UIAlertAction.init(title: two, style: .default, handler: { (action) in
            self.alertTime = two
        }))
        alert.addAction(UIAlertAction.init(title: three, style: .default, handler: { (action) in
            self.alertTime = three
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func AddBu(_ sender: UIButton) {
        if let api_token = ApiToken.getApiToken() {
            
            if repeatDate == nil {
                repeatDate = "nil"
            }
            if alertTime == nil {
                alertTime = "nil"
            }

            ApiMethods.AddScedual(api_token: api_token, name: self.NameText.text, location: self.LocationText.text, start: self.startDate!, end: self.endDate!, repeataction: repeatDate, alert: self.alertTime) { (error, status) in
                if status == 0 {
                    let message = "Unknown Error Please Try Again"
                    let title  = "Error"
                    self.AlertAction(message: message,title: title)
                }else {
               
                    let message = "Appointment Scheduled"
                    let title  = "Congratulations"
                    self.AlertAction(message: message , title: title)
                    
                }

            }
        }

    }
    func AlertAction(message : String, title : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
    }

    
}
