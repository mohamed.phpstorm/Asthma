//
//  LogoutVC.swift
//  Asthma
//
//  Created by MOHAMED on 7/2/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit

class LogoutVC: UIViewController {
    @IBOutlet var MainView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let Tap = UITapGestureRecognizer(target: self, action: #selector(LogoutVC.CloseWindow))
        // Do any additional setup after loading the view.
        self.MainView.addGestureRecognizer(Tap)
    }
    func CloseWindow() {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func LogoutAction(_ sender: UIButton) {
        
            if let api_token = ApiToken.getApiToken() {
                UserDefaults.standard.removeObject(forKey: "api_token")
                UserDefaults.standard.removeObject(forKey: "User_id")
                UserDefaults.standard.removeObject(forKey: "last_login")
                self.LoginScreen()
                
            }
        
        
    }
    func LoginScreen(){
        ApiToken.restartApp()
    }
}
