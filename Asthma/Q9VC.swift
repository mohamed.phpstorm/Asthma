//
//  Q9VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/14/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q9VC: UIViewController {
  
    var QuestionAnswer9 : String!

    var QuestionNineArray = [String]()
    // Button Atrr
    
    @IBOutlet weak var ButtonOne: UIButton! // 40
    @IBOutlet weak var ButtonTwo: UIButton! // 41
    @IBOutlet weak var ButtonThree: UIButton! // 42
    
    @IBOutlet weak var ButtonFour: UIButton! // 216
    
    @IBOutlet weak var ButtnSex: UIButton!//217
    
    @IBOutlet weak var ButtonFive: UIButton! //215
    @IBOutlet weak var TabButtonView: UIView!
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
       
        ApiMethods.GetQuestionAnswer(Question_id: "8"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionNineArray.append(obj!)

                    let FirstValue = "215"
                    let SeconValue = "40"
                    let ThirdValue = "42"
                    let FourthValue = "216"
                    let FifthValue = "41"
                    let SexValue = "217"
                    
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtnSex, Value: SexValue)
                     }
                    /*
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionNineArray)
            
        }
        
        
    }
    
    @IBAction func OCS(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "215")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionNineArray, Value: "215") { MyArray in
            
            self.QuestionNineArray = MyArray
            
        }
    }
    
    @IBAction func NonOcs(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "40")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionNineArray, Value: "40") { MyArray in
            
            self.QuestionNineArray = MyArray
            
        }
    }
    
    @IBAction func Other(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "42")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionNineArray, Value: "42") { MyArray in
            
            self.QuestionNineArray = MyArray
            
        }
    }
    @IBAction func ButtonFiveAction(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "41")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionNineArray, Value: "41") { MyArray in
            
            self.QuestionNineArray = MyArray
            
        }
    }
    
    @IBAction func ButtonFourActionBu(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "214")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionNineArray, Value: "214") { MyArray in
            
            self.QuestionNineArray = MyArray
            
        }
    }
    
    @IBAction func ButtonSexActionBu(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "216")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionNineArray, Value: "216") { MyArray in
            
            self.QuestionNineArray = MyArray
            
        }
    }
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionNineAnswer", Value: [])
        ApiToken.SetItems(Key: "QuestionNineComment", Value: "")

    }
    
    @IBAction func NextActionBu(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionNineAnswer", Value: QuestionNineArray)
        ApiToken.SetItems(Key: "QuestionNineComment", Value: self.QuestionComment.text)
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionNineAnswer", Value: QuestionNineArray)
        ApiToken.SetItems(Key: "QuestionNineComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    func QuestionAnswer(Button : UIButton , Value : String)  {
        self.QuestionAnswer9 = Value
        self.NextButton.isHidden = false

    }
    
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }

}




