//
//  Q18VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q18VC: UIViewController {

    var QuestionAnswer18 : String!
    var QuestionEighteenArray = [String]()
    
    
    @IBOutlet weak var ButtonOne: UIButton! // 81
    
    @IBOutlet weak var ButtonTwo: UIButton!  // 82
    
    @IBOutlet weak var ButtonThree: UIButton!  // 83
    
    @IBOutlet weak var ButtonFour: UIButton!  // 84
    
    @IBOutlet weak var ButtonFive: UIButton!  // 85
    
    @IBOutlet weak var ButtonSex: UIButton! // 86
    
    @IBOutlet weak var ButtonSeven: UIButton! //87
    
    @IBOutlet weak var ButtonEight: UIButton!  //88
    
    @IBOutlet weak var ButtonNine: UIButton! //221
    
    
    
    
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true

        ApiMethods.GetQuestionAnswer(Question_id: "21"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionEighteenArray.append(obj!)

                    let FirstValue = "81"
                    let SeconValue = "82"
                    let ThirdValue = "83"
                    let FourthValue = "84"
                    let FifthValue = "85"
                    let SexValue = "86"
                    let SevnthValue = "87"
                    let Eightvalue = "88"
                    let NineValue = "221"
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                    // Button Nine
                    if  obj == Eightvalue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonNine, Value: NineValue)
                    }
                }
            }
            print("MyTest Array :-" , self.QuestionEighteenArray)
            
        }
        
        
    }
    @IBAction func NumberOne(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "81")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEighteenArray, Value: "81") { MyArray in
            
            self.QuestionEighteenArray = MyArray
            
        }
    }
    
    @IBAction func NumberTwo(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "82")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEighteenArray, Value: "82") { MyArray in
            
            self.QuestionEighteenArray = MyArray
            
        }

    }
    @IBAction func NumberThree(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "83")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEighteenArray, Value: "83") { MyArray in
            
            self.QuestionEighteenArray = MyArray
            
        }

    }
    
    @IBAction func NumberFour(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "84")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEighteenArray, Value: "84") { MyArray in
            
            self.QuestionEighteenArray = MyArray
            
        }

    }
    
    @IBAction func NumberFive(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "85")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEighteenArray, Value: "85") { MyArray in
            
            self.QuestionEighteenArray = MyArray
            
        }

    }
    
    @IBAction func NumberSex(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "86")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEighteenArray, Value: "86") { MyArray in
            
            self.QuestionEighteenArray = MyArray
            
        }

    }
    
    @IBAction func NumberSeven(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "87")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEighteenArray, Value: "87") { MyArray in
            
            self.QuestionEighteenArray = MyArray
            
        }
    }
    
    
    @IBAction func NumberEight(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "88")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEighteenArray, Value: "88") { MyArray in
            
            self.QuestionEighteenArray = MyArray
            
        }
    }
    
    @IBAction func ButtonNine(_ sender: UIButton) {
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEighteenArray, Value: "221") { MyArray in
            
            self.QuestionEighteenArray = MyArray
            
        }
    }
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionEighteenAnswer", Value: [])
        
        ApiToken.SetItems(Key: "QuestionEighteenComment", Value: "")
    }
    
    @IBAction func NextButtonAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionEighteenAnswer", Value: QuestionEighteenArray)
        ApiToken.SetItems(Key: "QuestionEighteenComment", Value: self.QuestionComment.text)
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionEighteenAnswer", Value: QuestionEighteenArray)
        ApiToken.SetItems(Key: "QuestionEighteenComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer18 = Value
        self.NextButton.isHidden = false
        
        
    }
    
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
}




