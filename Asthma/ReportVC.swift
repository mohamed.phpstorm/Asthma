//
//  ReportVC.swift
//  Asthma
//
//  Created by MOHAMED on 6/30/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import SwiftyJSON
class ReportVC: UIViewController {
    var Hcp_Id : String!
    
    @IBOutlet weak var UserImage: UIButton!
    @IBOutlet weak var UserNameText: UILabel!
    
    @IBOutlet weak var LastLoginText: UILabel!
    @IBOutlet weak var FirstView: UIView!
    
    @IBOutlet weak var FirstAnswer: UILabel!
    
    @IBOutlet weak var FirstImage: UIImageView!
    
    @IBOutlet weak var SecondView: UIView!
    @IBOutlet weak var SecondImage: UIImageView!
    
    @IBOutlet weak var SecondAnswer: UILabel!
    @IBOutlet weak var ThirdAnswer: UILabel!
    
    @IBOutlet weak var FourthAnswer: UILabel!
    
    @IBOutlet weak var FifthAnswer: UILabel!
    
    @IBOutlet weak var ThirdView: UIView!
    
    @IBOutlet weak var ThirdImage: UIImageView!
    @IBOutlet weak var SexAnswer: UILabel!
    
    @IBOutlet weak var SeventhAnswer: UILabel!
    
    @IBOutlet weak var EightAnswer: UILabel!
    
    @IBOutlet weak var FourthView: UIView!
    
    @IBOutlet weak var NineAnswer: UILabel!
    
    @IBOutlet weak var TenAnswer: UILabel!
    
    
    @IBOutlet weak var FifthView: UIView!
    
    @IBOutlet weak var ElevenAnswer: UILabel!
    
    @IBOutlet weak var FifthImage: UIImageView!
    @IBOutlet weak var FourthImage: UIImageView!
    
    @IBOutlet weak var SexView: UIView!
    
    @IBOutlet weak var TwelveAnswer: UILabel!
    
    @IBOutlet weak var ThirteenAnswer: UILabel!
    
    @IBOutlet weak var FourteenAnswer: UILabel!
    
    @IBOutlet weak var FifteenAnswer: UILabel!
    
    @IBOutlet weak var SexImage: UIImageView!
    
    @IBOutlet weak var SevenView: UIView!
    
    @IBOutlet weak var SexteenAnswer: UILabel!
    
    @IBOutlet weak var SeventeenAnswer: UILabel!
    @IBOutlet weak var EighteenAnswer: UILabel!
    
    @IBOutlet weak var SevenImage: UIImageView!
    
    @IBOutlet weak var EightView: UIView!
    
    @IBOutlet weak var NinteenAnswer: UILabel!
    
    @IBOutlet weak var TwentyAnswer: UILabel!
    
    @IBOutlet weak var EightImage: UIImageView!
    
    @IBOutlet weak var NineView: UIView!
    
    @IBOutlet weak var TwentyOneAnswer: UILabel!
    @IBOutlet weak var TwentyTwoAnswer: UILabel!
    
    @IBOutlet weak var TwentyThreeAnswer: UILabel!
    
    @IBOutlet weak var TwentyFourAnswer: UILabel!
    
    var Views = ["FirstView","SecondView","ThirdView","FourthView","FifthView","SexView","SevenView","EightView","NineView"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.GetMedicalRepData(api_token: api_token){ (error , last_login , name , photo) in
                if error == nil {
                    self.LastLoginText.text = last_login
                    self.UserNameText.text = name
                    if photo != nil {
                    let ImageUrl = URL(string: ImageRoot + photo!)!
                    self.UserImage.kf.setImage(with: ImageUrl, for: .normal)
                    }else {
                        self.UserImage.setImage(#imageLiteral(resourceName: "Avatar"), for: .normal)
                    }
                }
            }
        }
        self.SecondView.isHidden = true
        self.ThirdView.isHidden = true
        self.FourthView.isHidden = true
        self.FifthView.isHidden = true
        self.SexView.isHidden = true
        self.SevenView.isHidden = true
        self.EightView.isHidden = true
        self.NineView.isHidden = true
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.GetPatientJourney(api_token: api_token , doctor_id: Hcp_Id) {(error,FirstAnswer ,SecondAnswer ,ThirdAnswer ,FourthAnswer ,FifthAnswer , SexAnswer ,SevenAnswer , EightAnswer , NineAnswer , TenAnswer , ElevenAnswer ,TwelveAnswer , ThirteenAnswer , FourteenAnswer, FifteenAnswer,SexteenAnswer ,SeventeenAnswer , EighteenAnswer, NinteenAnswer , TwentyAnswer ,TwentyOneAnswer, TwentyTwoAnswer , TwentyThreeAnswer ,TwentyFourAnswer ) in
                
                
                var Eleventext = ""
/*
                 if Eleventext.isEmpty {
                 Eleventext = eleven.string!
                 } else {
                 TenText += " || \(eleven)"
                 }

 */
                print("Second Answer Array :-" , SecondAnswer)
                
                
                var ErrorText = "Please Complete Patient Journey Questions First"

                if ElevenAnswer != nil {
                for obj in ElevenAnswer! {
                    if let obj = obj["answer"].array {
                        let firstarray = obj[0][0]
                        let seconarray = obj[1][0]
                        let getanswer = firstarray["get_answer"]
                        let MainTitle : String! = getanswer["title"].string
                        let textanswer = seconarray["parent_answer"]
                        let subtitle : String! = textanswer["title"].string
                        if Eleventext.isEmpty {
                            Eleventext = MainTitle + "|" + subtitle
                        } else {
                            Eleventext += " || \(MainTitle + "|" + subtitle)"
                        }
                    
                    }
                }
                } else {
                    Eleventext = ErrorText
                }
                var FirstText = ""
                var SecondText = ""
                var ThirdText = ""
                var FourthText = ""
                var FifthText = ""
                var SexText = ""
                var SevenText = ""
                var EightText = ""
                var NineText = ""
                var TenText = ""
                var TwelveText = ""
                var ThirteenText = ""
                var FourteenText = ""
                var FifteenText = ""
                var SexteenText = ""
                var SeventeenText = ""
                var EighteenText = ""
                var NinteenText = ""
                var TwentyText = ""
                var TwentyoneText = ""
                var TwentytwoText = ""
                var TwentythreeText = ""
                var TwentyFourText = ""
                // First Answers
                if FirstAnswer != nil {
                for first in FirstAnswer! {
                    if FirstText.isEmpty {
                        FirstText = first.string!
                    } else {
                        FirstText += " || \(first)"
                    }
                }
                } else {
                    FirstText = ErrorText
                }
                // Second Answers
                if SecondAnswer != nil {
                for second in SecondAnswer! {
                    if SecondText.isEmpty {
                        SecondText = second.string!
                    } else {
                        SecondText += " || \(second)"
                    }
                }
                } else {
                    SecondText = ErrorText
                }
                // Third Answers 
                if ThirdAnswer != nil {
                for three in ThirdAnswer! {
                    if ThirdText.isEmpty {
                        ThirdText = three.string!
                    } else {
                        ThirdText += " || \(three)"
                    }
                }
                } else {
                    ThirdText = ErrorText
                    
                }
                
                // Fourth Answer
                if FourthAnswer != nil {
                for four in FourthAnswer! {
                    if FourthText.isEmpty {
                        FourthText = four.string!
                    } else {
                        FourthText += " || \(four)"
                    }
                }
                } else {
                    FourthText = ErrorText
                }
                // Fifth Answer
                if FifthAnswer != nil {
                for five in FifthAnswer! {
                    if FifthText.isEmpty {
                        FifthText = five.string!
                    } else {
                        FifthText += " || \(five)"
                    }
                }
                } else {
                   FifthText = ErrorText
                }
                // Sex Answer
                if SexAnswer != nil {
                for sex in SexAnswer! {
                    if SexText.isEmpty {
                        SexText = sex.string!
                    } else {
                        SexText += " || \(sex)"
                    }
                }
                } else {
                    SexText = ErrorText
                }
                // Seven Answer 
                if SevenAnswer != nil {
                for seven in SevenAnswer! {
                    if SevenText.isEmpty {
                        SevenText = seven.string!
                    } else {
                        SevenText += " || \(seven)"
                    }
                }
                } else {
                    SevenText = ErrorText
                }
                // Eight Text
                if EightAnswer != nil {
                for eight in EightAnswer! {
                    if SevenText.isEmpty {
                        EightText = eight.string!
                    } else {
                        EightText += " || \(eight)"
                    }
                }
                } else {
                    EightText = ErrorText
                }
                // Nine Answer
                if NineAnswer != nil {
                for nine in NineAnswer! {
                    if NineText.isEmpty {
                        NineText = nine.string!
                    } else {
                        NineText += " || \(nine)"
                    }
                }
                } else {
                    NineText = ErrorText
                }
                //Ten Answer
                if TenAnswer != nil {
                for ten in TenAnswer! {
                    if NineText.isEmpty {
                        TenText = ten.string!
                    } else {
                        TenText += " || \(ten)"
                    }
                }
                } else {
                    TenText = ErrorText
                }
                // Twelve Answer
                if TwelveAnswer != nil {
                for twelve in TwelveAnswer! {
                    if TwelveText.isEmpty {
                        TwelveText = twelve.string!
                    } else {
                        TwelveText += " || \(twelve)"
                    }
                }
                } else {
                    TwelveText = ErrorText
                }
                // Thireen Answer
                if ThirteenAnswer != nil {
                
                for thirteen in ThirteenAnswer! {
                    if ThirteenText.isEmpty {
                        ThirteenText = thirteen.string!
                    } else {
                        ThirteenText += " || \(thirteen)"
                    }
                }
                } else {
                    ThirteenText = ErrorText
                }
                // Fourteen Answer
                if FourteenAnswer != nil {
                for fourteen in FourteenAnswer! {
                    if FourteenText.isEmpty {
                        FourteenText = fourteen.string!
                    } else {
                        FourteenText += " || \(fourteen)"
                    }
                }
                } else {
                    FourteenText = ErrorText
                }
                // Fifteen Answer
                if FifteenAnswer != nil {
                for fifteen in FifteenAnswer! {
                    if FifteenText.isEmpty {
                        FifteenText = fifteen.string!
                    } else {
                        FifteenText += " || \(fifteen)"
                    }
                }
                } else {
                    FifthText = ErrorText
                }
                // Sexteen Answer
                if SexteenAnswer != nil {
                for sexteen in SexteenAnswer! {
                    if SexteenText.isEmpty {
                        SexteenText = sexteen.string!
                    } else {
                        SexteenText += " || \(sexteen)"
                    }
                }
                } else {
                    SexteenText = ErrorText
                }
                // Seventeen Answer 
                if SeventeenAnswer != nil {
                for seventeen in SeventeenAnswer! {
                    if SeventeenText.isEmpty {
                        SeventeenText = seventeen.string!
                    } else {
                        SeventeenText += " || \(seventeen)"
                    }
                }
                } else {
                    SexteenText = ErrorText
                }
                // Eighteen Answer
                if EightAnswer != nil {
                    
                
                for eighteen in EighteenAnswer! {
                    if EighteenText.isEmpty {
                        EighteenText = eighteen.string!
                    } else {
                        EighteenText += " || \(eighteen)"
                    }
                }
                } else {
                    EightText = ErrorText
                }
                // Ninteen Answer
                
                if NinteenAnswer != nil {
                for ninteen in NinteenAnswer! {
                    if EighteenText.isEmpty {
                        NinteenText = ninteen.string!
                    } else {
                        NinteenText += " || \(ninteen)"
                    }
                }
                } else {
                    NineText = ErrorText
                }
                // Twenty Answer
                if TwentyAnswer != nil {
                for twenty in TwentyAnswer! {
                    if TwentyText.isEmpty {
                        TwentyText = twenty.string!
                    } else {
                        TwentyText += " || \(twenty)"
                    }
                }
                } else {
                    TwentyText = ErrorText
                }
                // Twenty One 
                if TwentyOneAnswer != nil {
                for twentyone in TwentyOneAnswer! {
                    print(twentyone)
                    if TwentyoneText.isEmpty {
                        TwentyoneText = twentyone.string!
                    } else {
                        TwentyoneText += " || \(twentyone)"
                    }
                }
                } else {
                    TwentyoneText = ErrorText
                }
                // Twenty Two 
                if TwentyTwoAnswer != nil {
                for twentytwo in TwentyTwoAnswer! {
                    if TwentytwoText.isEmpty {
                        TwentytwoText = twentytwo.string!
                    } else {
                        TwentytwoText += " || \(twentytwo)"
                    }
                }
                } else {
                    TwentytwoText = ErrorText
                }
                // Twenty Three 
                if TwentyThreeAnswer != nil {
                for twentythree in TwentyThreeAnswer! {
                    if TwentythreeText.isEmpty {
                        TwentythreeText = twentythree.string!
                    } else {
                        TwentythreeText += " || \(twentythree)"
                    }
                }
                } else {
                    TwentythreeText = ErrorText
                }
                // TwentyFour Answer 
                if TwentyFourAnswer != nil {
                for twentyfour in TwentyFourAnswer! {
                    if TwentythreeText.isEmpty {
                        TwentyFourText = twentyfour.string!
                    } else {
                        TwentyFourText += " || \(twentyfour)"
                    }
                }
                } else {
                    TwentyFourText = ErrorText
                }
                self.FirstAnswer.text = FirstText
                
                
                
                 self.SecondAnswer.text = SecondText
                print("Second Text :" , SecondText)
                 self.ThirdAnswer.text = ThirdText
                 
                 self.FourthAnswer.text = FourthText
                 
                 self.FifthAnswer.text = FifthText
                 
                 self.SexAnswer.text = SexText
                 
                 self.SeventhAnswer.text = SevenText
                 
                 self.EightAnswer.text = EightText
                 
                 
                 self.NineAnswer.text = NineText
                 
                 self.TenAnswer.text = TenText
                 
                 
                 
                 self.ElevenAnswer.text = Eleventext
                 
                 
                 self.TwelveAnswer.text = TwelveText
                 
                 
                 self.ThirteenAnswer.text = ThirdText
                 
                 self.FourteenAnswer.text = FourthText
                 
                 self.FifteenAnswer.text = FifteenText
                 
                 self.SexteenAnswer.text = SexteenText
                 
                 self.SeventeenAnswer.text = SevenText
                 
                 self.EighteenAnswer.text = EighteenText
                 
                 
                 
                 self.NinteenAnswer.text = NinteenText
                 
                 self.TwentyAnswer.text = TwentyText
                 
                 self.TwentyOneAnswer.text = TwentyoneText
                 self.TwentyTwoAnswer.text = TwentytwoText
                 
                 self.TwentyThreeAnswer.text = TwentythreeText
                 self.TwentyFourAnswer.text = TwentyFourText
                
                
            }
        }
        
        
        
    }
    
    var i = 0
    @IBAction func NextAction(_ sender: UIButton) {
        self.i += 1
        if i == 1 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = false
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = true
            self.FifthView.isHidden = true
            self.SexView.isHidden = true
            self.SevenView.isHidden = true
            self.EightView.isHidden = true
            self.NineView.isHidden = true
        }
        if i == 2 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = false
            self.FourthView.isHidden = true
            self.FifthView.isHidden = true
            self.SexView.isHidden = true
            self.SevenView.isHidden = true
            self.EightView.isHidden = true
            self.NineView.isHidden = true
        }
        if i == 3 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = false
            self.FifthView.isHidden = true
            self.SexView.isHidden = true
            self.SevenView.isHidden = true
            self.EightView.isHidden = true
            self.NineView.isHidden = true
        }
        if i == 4 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = true
            self.FifthView.isHidden = false
            self.SexView.isHidden = true
            self.SevenView.isHidden = true
            self.EightView.isHidden = true
            self.NineView.isHidden = true
        }
        if i == 5 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = true
            self.FifthView.isHidden = true
            self.SexView.isHidden = false
            self.SevenView.isHidden = true
            self.EightView.isHidden = true
            self.NineView.isHidden = true
        }
        if i == 6 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = true
            self.FifthView.isHidden = true
            self.SexView.isHidden = true
            self.SevenView.isHidden = false
            self.EightView.isHidden = true
            self.NineView.isHidden = true
        }
        if i == 7 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = true
            self.FifthView.isHidden = true
            self.SexView.isHidden = true
            self.SevenView.isHidden = true
            self.EightView.isHidden = false
            self.NineView.isHidden = true
        }
        if i == 8 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = true
            self.FifthView.isHidden = true
            self.SexView.isHidden = true
            self.SevenView.isHidden = true
            self.EightView.isHidden = true
            self.NineView.isHidden = false
        }
        if i > 8 {
            i = 8
        }
    }
    @IBAction func PreviousAction(_ sender: UIButton) {
        self.i -= 1
        if i < 0 {
            i = 0
        }
        if i == 0 {
            self.FirstView.isHidden = false
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = true
            self.FifthView.isHidden = true
            self.SexView.isHidden = true
            self.SevenView.isHidden = true
            self.EightView.isHidden = true
            self.NineView.isHidden = true
        }
        if i == 1 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = false
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = true
            self.FifthView.isHidden = true
            self.SexView.isHidden = true
            self.SevenView.isHidden = true
            self.EightView.isHidden = true
            self.NineView.isHidden = true
        }
        if i == 2 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = false
            self.FourthView.isHidden = true
            self.FifthView.isHidden = true
            self.SexView.isHidden = true
            self.SevenView.isHidden = true
            self.EightView.isHidden = true
            self.NineView.isHidden = true
        }
        if i == 3 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = false
            self.FifthView.isHidden = true
            self.SexView.isHidden = true
            self.SevenView.isHidden = true
            self.EightView.isHidden = true
            self.NineView.isHidden = true
        }
        if i == 4 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = true
            self.FifthView.isHidden = false
            self.SexView.isHidden = true
            self.SevenView.isHidden = true
            self.EightView.isHidden = true
            self.NineView.isHidden = true
        }
        if i == 5 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = true
            self.FifthView.isHidden = true
            self.SexView.isHidden = false
            self.SevenView.isHidden = true
            self.EightView.isHidden = true
            self.NineView.isHidden = true
        }
        if i == 6 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = true
            self.FifthView.isHidden = true
            self.SexView.isHidden = true
            self.SevenView.isHidden = false
            self.EightView.isHidden = true
            self.NineView.isHidden = true
        }
        if i == 7 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = true
            self.FifthView.isHidden = true
            self.SexView.isHidden = true
            self.SevenView.isHidden = true
            self.EightView.isHidden = false
            self.NineView.isHidden = true
        }
        if i == 8 {
            self.FirstView.isHidden = true
            self.SecondView.isHidden = true
            self.ThirdView.isHidden = true
            self.FourthView.isHidden = true
            self.FifthView.isHidden = true
            self.SexView.isHidden = true
            self.SevenView.isHidden = true
            self.EightView.isHidden = true
            self.NineView.isHidden = false
        }
    }
    
    @IBAction func MenuBuAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuVC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    @IBAction func NotificationAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SchedualScreen") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func LogoutAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutScreen") as! LogoutVC
        self.present(vc, animated: true, completion: nil)
    }
    
    
    
    
    
    
}
