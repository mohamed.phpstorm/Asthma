//
//  Q3VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/14/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q3VC: UIViewController {



    var QuestionAnswer3 : String!
    
    var QuestionThreeArray = [String]()
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    // Button Atrr
    
    @IBOutlet weak var ButtonOne: UIButton! // 13
    
    @IBOutlet weak var ButtonTwo: UIButton! //14
    
    @IBOutlet weak var ButtonThree: UIButton! // 15
    
    
    @IBOutlet weak var ButtonFour: UIButton! //16
    
    @IBOutlet weak var ButtonFive: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true

        ApiMethods.GetQuestionAnswer(Question_id: "3"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionThreeArray.append(obj!)

                    let FirstValue = "13"
                    let SeconValue = "14"
                    let ThirdValue = "15"
                    let FourthValue = "16"
                                let FifthValue  = "212"
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                   
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     /*
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionThreeArray)
            
        }
        
        
    }
    
    @IBAction func GinaStep5(_ sender: UIButton) {
        
        QuestionAnswer(Button: sender, Value: "13")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThreeArray, Value: "13") { MyArray in
            
            self.QuestionThreeArray = MyArray
            
        }
    }
    
    
    
    @IBAction func GinaStep4(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "14")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThreeArray, Value: "14") { MyArray in
            
            self.QuestionThreeArray = MyArray
            
        }
    }
    
    
    @IBAction func PatientBu(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "15")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThreeArray, Value: "15") { MyArray in
            
            self.QuestionThreeArray = MyArray
            
        }
    }
    
    
    
    @IBAction func OtherBu(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "16")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThreeArray, Value: "16") { MyArray in
            
            self.QuestionThreeArray = MyArray
            
        }
        
    }
    
    @IBAction func ButtonFiveActionBu(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "212")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThreeArray, Value: "212") { MyArray in
            
            self.QuestionThreeArray = MyArray
            
        }
    }
    @IBAction func SkipActionBu(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThreeAnswer", Value: [])
        print("Ok")
        
        ApiToken.SetItems(Key: "QuestionThreeComment", Value: "")
        
    }

    @IBAction func NextActionBu(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThreeAnswer", Value: QuestionThreeArray)
        ApiToken.SetItems(Key: "QuestionThreeComment", Value: self.QuestionComment.text)
    }
    
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThreeAnswer", Value: QuestionThreeArray)
        ApiToken.SetItems(Key: "QuestionThreeComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
            }
            
        }
    }
    
    func QuestionAnswer(Button : UIButton , Value : String)  {
        self.QuestionAnswer3 = Value
        self.NextButton.isHidden = false
        
    }
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
}



