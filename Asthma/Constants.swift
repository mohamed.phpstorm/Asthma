//
//  Constants.swift
//  Asthma
//
//  Created by MOHAMED on 6/5/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

let MainUrl = "http://tech.techno-hat.com/severe_asthma/api/v1/"

let PdfArtilcUrl = "http://tech.techno-hat.com/severe_asthma/"
let RegisterUrl = MainUrl + "register"

let LoginUrl = MainUrl + "login"

let RegisterNotifyUrl = MainUrl + "register-token"


let fileRoot = "http://tech.techno-hat.com/severe_asthma/"

let HcpDoctorUrl = MainUrl + "my-doctors"

let AddDoctorUrl = MainUrl + "new-doctor"

let DoctorDetails = MainUrl + "doctor/"

let EditDoctorUrl = MainUrl + "update-doctor/"

let ResetPasswordUrl = MainUrl + "reset-password"

let DeleteDoctorUrl = MainUrl + "delete-doctor/"

let GetPdfUrl = MainUrl + "questionnaire-by-doctor"
let GetModerateUrl = MainUrl + "mild/moderate"

let AddQuestionierUrl = MainUrl + "create-questionnaire"

let GetArticelsUrl = MainUrl + "articles"

let AddSchedualUrl = MainUrl + "create-reminder"

let ReminderListUrl = MainUrl + "reminders?api_token="

let GetReminderByUserUrl = MainUrl + "reminders/by/user"

let HcpQuestionierUrl = MainUrl + "my-doctors-for-questionnaire"

let CountriesUrl = MainUrl + "countries"
let CitiesUrl = MainUrl + "cities"
let HcpPatientJourneyUrl = MainUrl + "my-doctors-have-questionnaire"
let ImageRoot = "http://tech.techno-hat.com/severe_asthma/"
let PatientJourneyUrl = MainUrl + "patient-journey"
let SingleQuestionUrl = MainUrl + "retrive/questions"
let UserDataUrl = "http://tech.techno-hat.com/severe_asthma/api/user"
let AddModUrl = MainUrl + "mild/moderate"
