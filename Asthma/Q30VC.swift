//
//  Q30VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q30VC: UIViewController {
    
    var QuestionAnswer30 : String!
    var QuestionThirtyArray = [String]()
    
    
    @IBOutlet weak var ButtonOne: UIButton!  //142
    
    @IBOutlet weak var ButtonTwo: UIButton!  //143
    
    @IBOutlet weak var ButtonThree: UIButton!  //144
    
    @IBOutlet weak var ButtonFour: UIButton!  //145
    
    @IBOutlet weak var ButtonFive: UIButton!  //146
    

    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
        ApiMethods.GetQuestionAnswer(Question_id: "33"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionThirtyArray.append(obj!)

                    let FirstValue = "142"
                    let SeconValue = "143"
                    let ThirdValue = "144"
                    let FourthValue = "145"
                    let FifthValue = "146"
               
                    
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }/*
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionThirtyArray)
            
        }
        
        
    }
    
    @IBAction func NumberOne(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "142")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyArray, Value: "142") { MyArray in
            
            self.QuestionThirtyArray = MyArray
            
        }
    }
    
    @IBAction func NumberTwo(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "143")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyArray, Value: "143") { MyArray in
            
            self.QuestionThirtyArray = MyArray
            
        }
    }
    
    @IBAction func NumberThree(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "144")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyArray, Value: "144") { MyArray in
            
            self.QuestionThirtyArray = MyArray
            
        }

    }
    
    @IBAction func NumberFour(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "145")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyArray, Value: "145") { MyArray in
            
            self.QuestionThirtyArray = MyArray
            
        }

    }
    @IBAction func NumberFive(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "146")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyArray, Value: "146") { MyArray in
            
            self.QuestionThirtyArray = MyArray
            
        }
    }
    
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionThirtyAnswer", Value: [])
        
        ApiToken.SetItems(Key: "QuestionThirtyComment", Value: "")
    }
    
    @IBAction func NextButtonAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirtyAnswer", Value: self.QuestionThirtyArray)
        ApiToken.SetItems(Key: "QuestionThirtyComment", Value: self.QuestionComment.text)
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirtyAnswer", Value: self.QuestionThirtyArray)
        ApiToken.SetItems(Key: "QuestionThirtyComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer30 = Value
        self.NextButton.isHidden = false
      
        
    }
    
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
}




