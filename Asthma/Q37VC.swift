//
//  Q37VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q37VC: UIViewController {
    
    
    var QuestionAnswer37 : String!
    
    var QuestionThirtySevenArray = [String]()
    
    
    @IBOutlet weak var ButtonOne: UIButton!  //179
    
    @IBOutlet weak var Buttontwo: UIButton!  //180
    
    @IBOutlet weak var ButtonThree: UIButton!  //181
    
    @IBOutlet weak var ButtonFour: UIButton!  //182
    
    @IBOutlet weak var ButtonFive: UIButton! //183
    
    @IBOutlet weak var ButtonSex: UIButton!  // 184
    
    @IBOutlet weak var ButtonSeven: UIButton!  //185
    
    @IBOutlet weak var ButtonEight: UIButton!  //225
    
    @IBOutlet weak var ButtonNine: UIButton!  //226
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
        ApiMethods.GetQuestionAnswer(Question_id: "40"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionThirtySevenArray.append(obj!)

                    let FirstValue = "179"
                    let SeconValue = "180"
                    let ThirdValue = "181"
                    let FourthValue = "182"
                    let FifthValue = "183"
                    let SexValue = "184"
                    let SevnthValue = "185"
                    let EightValue = "225"
                    let NineValue = "226"
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.Buttontwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }/*
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionThirtySevenArray)
            
        }
        
        
    }
    
    
    @IBAction func NumberOne(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "179")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtySevenArray, Value: "179") { MyArray in
            
            self.QuestionThirtySevenArray = MyArray
            
        }
    }
    
    @IBAction func NumberTwo(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "180")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtySevenArray, Value: "180") { MyArray in
            
            self.QuestionThirtySevenArray = MyArray
            
        }
    }
    
    @IBAction func NumberThree(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "181")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtySevenArray, Value: "181") { MyArray in
            
            self.QuestionThirtySevenArray = MyArray
            
        }
    }
    
    @IBAction func NumberFour(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "182")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtySevenArray, Value: "182") { MyArray in
            
            self.QuestionThirtySevenArray = MyArray
            
        }
    }
    
    
    @IBAction func NumberFive(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "183")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtySevenArray, Value: "183") { MyArray in
            
            self.QuestionThirtySevenArray = MyArray
            
        }
    }
    
    @IBAction func NumberSex(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "184")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtySevenArray, Value: "184") { MyArray in
            
            self.QuestionThirtySevenArray = MyArray
            
        }

    }
    
    @IBAction func NumberSeven(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "185")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtySevenArray, Value: "185") { MyArray in
            
            self.QuestionThirtySevenArray = MyArray
            
        }
    }
    
    @IBAction func EightNumber(_ sender: UIButton) {
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtySevenArray, Value: "225") { MyArray in
            
            self.QuestionThirtySevenArray = MyArray
            
        }
    }
    
    @IBAction func NineNumber(_ sender: UIButton) {
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtySevenArray, Value: "185") { MyArray in
            
            self.QuestionThirtySevenArray = MyArray
            
        }
    }
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionThirtySevenAnswer", Value: [])
        
        ApiToken.SetItems(Key: "QuestionThirtySevenComment", Value: "")
    }
    
    
    
    
    @IBAction func NextButtonAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirtySevenAnswer", Value: self.QuestionThirtySevenArray)
        ApiToken.SetItems(Key: "QuestionThirtySevenComment", Value: self.QuestionComment.text)
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirtySevenAnswer", Value: self.QuestionThirtySevenArray)
        ApiToken.SetItems(Key: "QuestionThirtySevenComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer37 = Value
        self.NextButton.isHidden = false

    }
    
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
}





