//
//  SeverAsthmaModel.swift
//  Asthma
//
//  Created by MOHAMED on 6/5/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import SwiftyJSON
class SeverAsthmaModel {
    var id : Int!
    var title : String!
    var details : String!
    var created_date : String!
    var image : String!
    var attachment : String!
    init(SeverDict : [String:JSON]) {
        if let ID = SeverDict["id"]?.int {
            self.id = ID
        }
        
        if let Title = SeverDict["title"]?.string {
            self.title = Title
        }
        
        if let Details = SeverDict["description"]?.string {
            self.details = Details
        }
        
        if let UploadDate = SeverDict["created_at"]?.string {
            self.created_date = UploadDate
        }
        
        if let Image = SeverDict["attachment_extension"]?.string {
            self.image = Image
        }
        if let Attachment = SeverDict["attachment"]?.string{
            self.attachment = Attachment
        }
        
    }
}
