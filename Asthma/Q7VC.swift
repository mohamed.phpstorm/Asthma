//
//  Q7VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/14/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q7VC: UIViewController {
    
    var QuestionAnswer7 : String!
  
    var QuestionSevenArray = [String]()
    
    // Button Atte
    @IBOutlet weak var ButtonOne: UIButton! // 31
    @IBOutlet weak var ButtonTwo: UIButton! //32
    @IBOutlet weak var ButtonThree: UIButton! //33
    @IBOutlet weak var ButtonFour: UIButton! // 34
    @IBOutlet weak var ButtonFive: UIButton! //35
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
        print("Answers" , ApiToken.GetDict(key: "QuestionFivePercentage")
)
        ApiMethods.GetQuestionAnswer(Question_id: "6"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionSevenArray.append(obj!)

                    let FirstValue = "31"
                    let SeconValue = "32"
                    let ThirdValue = "33"
                    let FourthValue = "34"
                    let FifthValue = "35"
                
                    
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }/*
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionSevenArray)
            
        }
        
        
    }
    
    
    @IBAction func FirstPre(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "31")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionSevenArray, Value: "31") { MyArray in
            
            self.QuestionSevenArray = MyArray
            
        }
    }
    
    @IBAction func LessThan(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "32")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionSevenArray, Value: "32") { MyArray in
            
            self.QuestionSevenArray = MyArray
            
        }
    }
    
    
    @IBAction func BetweenMon(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "33")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionSevenArray, Value: "33") { MyArray in
            
            self.QuestionSevenArray = MyArray
            
        }
    }
    
    
    @IBAction func ThreeMonths(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "34")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionSevenArray, Value: "34") { MyArray in
            
            self.QuestionSevenArray = MyArray
            
        }
    }
    
    
    @IBAction func Other(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "35")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionSevenArray, Value: "35") { MyArray in
            
            self.QuestionSevenArray = MyArray
            
        }
    }
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        
        ApiToken.SetArray(Key: "QuestionSevenAnswer", Value: [])
        ApiToken.SetItems(Key: "QuestionSevenComment", Value: "")

    }
    
    @IBAction func NextButtonAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionSevenAnswer", Value: QuestionSevenArray)
        ApiToken.SetItems(Key: "QuestionSevenComment", Value: self.QuestionComment.text)
    }
    
 
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionSevenAnswer", Value: QuestionSevenArray)
        ApiToken.SetItems(Key: "QuestionSevenComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    func QuestionAnswer(Button : UIButton , Value : String)  {
        self.QuestionAnswer7 = Value
        self.NextButton.isHidden = false
 
        
    }
    
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }

}




