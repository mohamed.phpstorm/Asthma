//
//  LoginVC.swift
//  Asthma
//
//  Created by MOHAMED on 5/29/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import Foundation
class LoginVC: UITableViewController {
    
    
    @IBOutlet weak var EmailText: UITextField!
    @IBOutlet weak var PasswordText: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.CustomizeTextField(Field: EmailText)
        self.CustomizeTextField(Field: PasswordText)
    }

    func CustomizeTextField(Field:UITextField){
        Field.backgroundColor = UIColor.black
        Field.layer.cornerRadius = 20.0
        let mycolor : UIColor = UIColor.white
        Field.layer.borderColor = mycolor.cgColor
        Field.layer.borderWidth = 2
        
    }
    
    @IBAction func LoginActionBu(_ sender: UIButton) {
        guard let email = self.EmailText.text , !email.isEmpty else {
        
            let message = "You Need To Fil Email Field"
            let title  = "Error"
            self.AlertAction(message: message,title: title)
            return
        }
        
        guard let password = self.PasswordText.text , !password.isEmpty  else {
            let message = "You Need To Fil Password Field"
            let title  = "Error"
            self.AlertAction(message: message,title: title)
            return
        }
        
        ApiMethods.MedicalRepLogin(email: self.EmailText.text!, password: self.PasswordText.text!){(status , message , error) in
            
            if (status == 1) {
                let message = "Logged In Succeful"
                let title = "Welcome"
                self.AlertAction(message: message, title: title)
                
            }else {
                let title = "Error"
                let message = "Incorrect Username Or Password"
                self.AlertAction(message: message, title: title)
            }
        }
        
    }
    
    
    func AlertAction(message : String, title : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
  }
