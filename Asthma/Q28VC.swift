//
//  Q28VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q28VC: UIViewController {
    
    var QuestionAnswer28 : String!
    var QuestionTwentyEight = [String]()
    
    
    
    @IBOutlet weak var ButtonOne: UIButton! //134
    
    @IBOutlet weak var ButtonTwo: UIButton!  //135
    
    @IBOutlet weak var ButtonThree: UIButton!  //136
    
    @IBOutlet weak var ButtonFour: UIButton!   //137
    
    
    
    
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
        ApiMethods.GetQuestionAnswer(Question_id: "31"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionTwentyEight.append(obj!)

                    let FirstValue = "134"
                    let SeconValue = "135"
                    let ThirdValue = "136"
                    let FourthValue = "137"
                   
                    
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    /*
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionTwentyEight)
            
        }
        
        
    }
    
    
    @IBAction func NumberOne(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "134")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentyEight, Value: "134") { MyArray in
            
            self.QuestionTwentyEight = MyArray
            
        }

    }
    
    
    @IBAction func NumberTwo(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "135")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentyEight, Value: "135") { MyArray in
            
            self.QuestionTwentyEight = MyArray
            
        }
    }
    
    @IBAction func NumberThree(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "136")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentyEight, Value: "136") { MyArray in
            
            self.QuestionTwentyEight = MyArray
            
        }
    }
    
    
    @IBAction func NumberFour(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "137")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentyEight, Value: "137") { MyArray in
            
            self.QuestionTwentyEight = MyArray
            
        }
    }
    
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionTwentyEightAnswer", Value: [])
        
        ApiToken.SetItems(Key: "QuestionTwentyEightComment", Value: "")
    }
    
   
    @IBAction func NextButtonAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionTwentyEightAnswer", Value: self.QuestionTwentyEight)
        ApiToken.SetItems(Key: "QuestionTwentyEightComment", Value: self.QuestionComment.text)
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionTwentyEightAnswer", Value: self.QuestionTwentyEight)
        ApiToken.SetItems(Key: "QuestionTwentyEightComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer28 = Value
        self.NextButton.isHidden = false
       
       
        
    }
    
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
}




