//
//  GetReminderCell.swift
//  Asthma
//
//  Created by MOHAMED on 8/17/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit

class GetReminderCell: UITableViewCell {

    @IBOutlet weak var ReminderDate: UILabel!
   
    @IBOutlet weak var ReminderTime: UILabel!

    @IBOutlet weak var ReminderName: UILabel!
    
    @IBOutlet weak var ReminderLocation: UILabel!
    
    @IBOutlet weak var ReminderAlert: UILabel!
    
    @IBOutlet weak var ReminderRepeat: UILabel!
    
    var checkRepeat : String!
    
    func ConfigureCell(List : ReminderList){
        self.ReminderName.text = List.name
        self.ReminderLocation.text = List.location
        self.ReminderAlert.text = List.start
       // self.checkRepeat = List.reminderRepeat
        
        
        
        
        self.ReminderAlert.isHidden = true
        
        if List.start != nil {
            // Customize Date To Time With am and Pm Format
            let time : String! = List.start
            let dateformatter = DateFormatter()
             dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
           
            var MyDate = dateformatter.date(from: time)
            dateformatter.dateFormat = "hh:mm a"
            dateformatter.timeZone = TimeZone(abbreviation: "GMT+3:00")

            var ConvertDate = dateformatter.string(from: MyDate!)
            self.ReminderTime.text = ConvertDate
            // Cutomize Date Formate With Month Name
            dateformatter.dateFormat = "YYYY MMM dd"
            
            var FullDate = dateformatter.string(from: MyDate!)
            self.ReminderDate.text = FullDate

            
            
        }
        if List.reminderRepeat != nil {
            self.checkRepeat = List.reminderRepeat
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            var RepeatDate = dateFormatter.date(from: self.checkRepeat)
            dateFormatter.dateFormat = "YYYY MMM dd"
            var RepeatString = dateFormatter.string(from: RepeatDate!)
            print("Check Repeat :- " , RepeatDate)
            self.ReminderRepeat.text = "Repeat " + RepeatString
        }else {
            self.ReminderRepeat.text = "There Is No Repeat"
        }
    }
    
}
