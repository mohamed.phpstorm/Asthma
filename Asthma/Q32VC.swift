//
//  Q32VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q32VC: UIViewController {

    var QuestionAnswer32 : String!
    
    var QuestionThirtyTwoArray = [String]()
    
    @IBOutlet weak var ButtonOne: UIButton!  // 153
    
    @IBOutlet weak var ButtonTwo: UIButton!  //154
    
    @IBOutlet weak var ButtonThree: UIButton!  //155
    
    @IBOutlet weak var ButtonFour: UIButton!  //156
    
    @IBOutlet weak var ButtonFive: UIButton!  //157
    
    @IBOutlet weak var ButtonSex: UIButton!   //158
    
    
    
    
    
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
    }
    
    
 
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionThirtyTwoAnswer", Value: [])
        
        ApiToken.SetItems(Key: "QuestionThirtyTwoComment", Value: "")
    }
    @IBAction func NextButton(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirtyTwoAnswer", Value: ["0"])
        ApiToken.SetItems(Key: "QuestionThirtyTwoComment", Value: self.QuestionComment.text)
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirtyTwoAnswer", Value:  ["0"])
        ApiToken.SetItems(Key: "QuestionThirtyTwoComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer32 = Value
        self.NextButton.isHidden = false
        
        
        
    }
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
}




