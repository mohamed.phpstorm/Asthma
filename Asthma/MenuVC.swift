//
//  MenuVC.swift
//  Asthma
//
//  Created by MOHAMED on 6/10/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit

class MenuVC: UIViewController {
    @IBOutlet var MainView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let Tap = UITapGestureRecognizer(target: self, action: #selector(MenuVC.CloseWindow))
        // Do any additional setup after loading the view.
        self.MainView.addGestureRecognizer(Tap)
    }
    func CloseWindow() {
        self.dismiss(animated: true, completion: nil)
    }
}
