//
//  Q4VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/14/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q4VC: UIViewController {

    var QuestionAnswer4 : String!
    var QuestionFourArray = [String]()
    
    
    
    @IBOutlet weak var ButtonOne: UIButton! // 17
    @IBOutlet weak var ButtonTwo: UIButton! // 18
    @IBOutlet weak var ButtonThree: UIButton! //19
    @IBOutlet weak var ButtonFour: UIButton! // 20
    
    @IBOutlet weak var ButtonFive: UIButton!
    @IBOutlet weak var ButtonSex: UIButton!
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true

        ApiMethods.GetQuestionAnswer(Question_id: "4"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionFourArray.append(obj!)

                    let FirstValue = "17"
                    let SeconValue = "18"
                    let ThirdValue = "19"
                    let FourthValue = "20"
                  let FifthValue = "213"
                    let SexValue = "214"
                    
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                    /*
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionFourArray)
            
        }
        
        
    }
    
    
    @IBAction func Hospitalization(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "17")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFourArray, Value: "17") { MyArray in
            
            self.QuestionFourArray = MyArray
            
        }
    }
    
    
    @IBAction func Ocs(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "18")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFourArray, Value: "18") { MyArray in
            
            self.QuestionFourArray = MyArray
            
        }
    }
    
    
    @IBAction func Lack(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "19")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFourArray, Value: "19") { MyArray in
            
            self.QuestionFourArray = MyArray
            
        }
    }
    
    @IBAction func Other(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "20")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFourArray, Value: "20") { MyArray in
            
            self.QuestionFourArray = MyArray
            
        }
    }
    
    @IBAction func ButtonFiveActionBu(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "213")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFourArray, Value: "213") { MyArray in
            
            self.QuestionFourArray = MyArray
            
        }
    }
    @IBAction func ButtonSexActionBu(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "214")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFourArray, Value: "214") { MyArray in
            self.QuestionFourArray = MyArray
            
        }
    }
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionFourAnswer", Value: [])
        print("Ok")
        
        ApiToken.SetItems(Key: "QuestionFourComment", Value: "")
        
    }
    
    @IBAction func NextActionBu(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionFourAnswer", Value: QuestionFourArray )
        ApiToken.SetItems(Key: "QuestionFourComment", Value: self.QuestionComment.text)
        
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionFourAnswer", Value: QuestionFourArray )
        ApiToken.SetItems(Key: "QuestionFourComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
            }
            
        }
    }
    
    func QuestionAnswer(Button : UIButton , Value : String)  {
        self.QuestionAnswer4 = Value
        self.NextButton.isHidden = false
        
    }
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
}



