//
//  Q8VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/14/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q8VC: UIViewController {
    var QuestionAnswer8 : String!
   
    var QuestionEightArray = [String]()
    
    // Button Atrr
    @IBOutlet weak var ButtonOne: UIButton!  // 36
    @IBOutlet weak var ButtonTwo: UIButton! // 37
    @IBOutlet weak var ButtonThree: UIButton!  // 38
    @IBOutlet weak var ButtonFour: UIButton! // 39
    
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true

        ApiMethods.GetQuestionAnswer(Question_id: "7"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionEightArray.append(obj!)

                    let FirstValue = "36"
                    let SeconValue = "37"
                    let ThirdValue = "38"
                    let FourthValue = "39"
                   
                    
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    /*
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionEightArray)
            
        }
        
        
    }
    @IBAction func TheOne(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "36")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEightArray, Value: "36") { MyArray in
            
            self.QuestionEightArray = MyArray
            
        }
    }
    
    @IBAction func TheTwo(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "37")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEightArray, Value: "37") { MyArray in
            
            self.QuestionEightArray = MyArray
            
        }
    }
    
    @IBAction func TheThree(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "38")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEightArray, Value: "38") { MyArray in
            
            self.QuestionEightArray = MyArray
            
        }
    }
    
    
    @IBAction func TheFour(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "39")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEightArray, Value: "39") { MyArray in
            
            self.QuestionEightArray = MyArray
            
        }

    }
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        
        ApiToken.SetArray(Key: "QuestionEightAnswer", Value: [])
        ApiToken.SetItems(Key: "QuestionEightComment", Value: "")

        
    }
    
    @IBAction func NextButtonAction(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionEightAnswer", Value: QuestionEightArray)
        ApiToken.SetItems(Key: "QuestionEightComment", Value: self.QuestionComment.text)
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionEightAnswer", Value: QuestionEightArray)
        ApiToken.SetItems(Key: "QuestionEightComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    func QuestionAnswer(Button : UIButton , Value : String)  {
        self.QuestionAnswer8 = Value
        self.NextButton.isHidden = false
        
    }
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }

}




