//
//  HCPModel.swift
//  Asthma
//
//  Created by MOHAMED on 6/6/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import Foundation
import SwiftyJSON

class HCPModel {
    var id : String!
    var name : String!
    
    init(HCPDict : [String:JSON]) {
        
        if let ID = HCPDict["id"]?.int {
            self.id = String(ID)
            print("ID :- " , ID)
        }
        
        if let Name = HCPDict["name"]?.string {
            self.name = Name
        }
         
        
    }
    
}
