
//  Q2VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/14/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q2VC: UIViewController {
    var Hcp_Id : String!

    var QuestionTwoArray = [String]()
    var QuestionAnswer2 : String!
    var QuestionAnswer1 : String!
    var QuestionComment1 : String!
    // Button Attr
    @IBOutlet weak var ButtonOne: UIButton! // 5
    @IBOutlet weak var ButtonTwo: UIButton! // 6
    @IBOutlet weak var ButtonThree: UIButton!// 7
   // @IBOutlet weak var ButtonFour: UIButton!// 8
    
    @IBOutlet weak var ButtonFive: UIButton! //8
    @IBOutlet weak var ButtonSex: UIButton! //9
    @IBOutlet weak var ButtonSeve: UIButton! // 10
    @IBOutlet weak var ButtonEight: UIButton! // 11
    @IBOutlet weak var ButtonNine: UIButton! //12
    
    
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
        ApiMethods.GetQuestionAnswer(Question_id: "2"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionTwoArray.append(obj!)

                    let FirstValue = "5"
                    let SeconValue = "6"
                    let ThirdValue = "7"
                    let FifthValue = "8"
                    let SexValue = "9"
                    let SevnthValue = "10"
                    let Eightvalue = "11"
                    let NineValue = "12"
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                  
                    
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeve, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     // Button Nine
                    if obj == NineValue {
                        ApiToken.CheckQuestionValue(Button: self.ButtonNine, Value: NineValue)
                    }
                }
            }
            print("MyTest Array :-" , self.QuestionTwoArray)
            
        }
        
        
    }
    
    @IBAction func ZeroToTen(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "5")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        // Add value To Array
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwoArray, Value: "5") { MyArray in
            
            self.QuestionTwoArray = MyArray
            
        }
    }
    
    
    @IBAction func TenToTwenty(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "6")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        // Add value To Array
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwoArray, Value: "6") { MyArray in
            
            self.QuestionTwoArray = MyArray
            
        }
    }
    
    @IBAction func TwentyToThirty(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "7")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        // Add value To Array
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwoArray, Value: "7") { MyArray in
            
            self.QuestionTwoArray = MyArray
            
        }

    }
    
    
    @IBAction func ThirtyToFourty(_ sender: UIButton) {
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        QuestionAnswer(Button: sender, Value: "8")
        // Add value To Array
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwoArray, Value: "8") { MyArray in
            
            self.QuestionTwoArray = MyArray
            
        }

    }
    
    @IBAction func FourtyToFifty(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "9")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        // Add value To Array
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwoArray, Value: "9") { MyArray in
            
            self.QuestionTwoArray = MyArray
            
        }

    }
    
    
    @IBAction func FiftyToSixty(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "10")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        // Add value To Array
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwoArray, Value: "10") { MyArray in
            
            self.QuestionTwoArray = MyArray
            
        }
    }
    
    
    @IBAction func AboveSixty(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "11")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwoArray, Value: "11") { MyArray in
            
            self.QuestionTwoArray = MyArray
            
        }
    }
    
    
    
    @IBAction func AllAges(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "12")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwoArray, Value: "12") { MyArray in
            
            self.QuestionTwoArray = MyArray
            
        }
    }
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionTwoAnswer", Value: [])
        
        ApiToken.SetItems(Key: "QuestionTwoComment", Value: "")
        
    }
    @IBAction func NextButtonAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionTwoAnswer", Value: QuestionTwoArray)
        print("Q2", self.QuestionAnswer2)
        ApiToken.SetItems(Key: "QuestionTwoComment", Value: self.QuestionComment.text)
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetItems(Key: "QuestionTwoComment", Value: self.QuestionComment.text)
        ApiToken.SetArray(Key: "QuestionTwoAnswer", Value: QuestionTwoArray)
        ApiToken.SetItems(Key: "QuestionTwoComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)

                }else {
                    RemoveData.removeDataObj()
                }
                

            }
            
        }
    }
    
    func QuestionAnswer(Button : UIButton , Value : String)  {
        self.QuestionAnswer2 = Value
        self.NextButton.isHidden = false
    
        
    }
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)

    }
    
}
