//
//  Q34VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q34VC: UIViewController {
    
    var QuestionAnswer34 : String!
    
    var QuestionThirtyFourArray = [String]()
    
    
    @IBOutlet weak var ButtonOne: UIButton!  //165
    
    @IBOutlet weak var ButtonTwo: UIButton!  //166
    
    @IBOutlet weak var ButtonThree: UIButton!  //167
    
    @IBOutlet weak var ButtonFour: UIButton! //168
    
    @IBOutlet weak var ButtonFive: UIButton!  //169
   
    @IBOutlet weak var ButtonSex: UIButton!  //170
    
    @IBOutlet weak var ButtonSeven: UIButton!  //171
    
    @IBOutlet weak var ButtonEight: UIButton!  //172
    
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
        ApiMethods.GetQuestionAnswer(Question_id: "37"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionThirtyFourArray.append(obj!)

                    let FirstValue = "165"
                    let SeconValue = "166"
                    let ThirdValue = "167"
                    let FourthValue = "168"
                    let FifthValue = "169"
                    let SexValue = "170"
                    let SevnthValue = "171"
                    let Eightvalue = "172"
                    
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                }
            }
            print("MyTest Array :-" , self.QuestionThirtyFourArray)
            
        }
        
        
    }    
    
    @IBAction func NumberOne(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "165")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyFourArray, Value: "165") { MyArray in
            
            self.QuestionThirtyFourArray = MyArray
            
        }
    }
    
    @IBAction func NumberTwo(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "166")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyFourArray, Value: "166") { MyArray in
            
            self.QuestionThirtyFourArray = MyArray
            
        }
    }
    
    @IBAction func NumberThree(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "167")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyFourArray, Value: "167") { MyArray in
            
            self.QuestionThirtyFourArray = MyArray
            
        }
    }
    
    @IBAction func NumberFour(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "168")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyFourArray, Value: "168") { MyArray in
            
            self.QuestionThirtyFourArray = MyArray
            
        }
    }
    
    @IBAction func NumberFive(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "169")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyFourArray, Value: "169") { MyArray in
            
            self.QuestionThirtyFourArray = MyArray
            
        }
    }
    
    @IBAction func NumberSex(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "170")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyFourArray, Value: "170") { MyArray in
            
            self.QuestionThirtyFourArray = MyArray
            
        }
    }
    
    @IBAction func NumberSeven(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "171")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyFourArray, Value: "171") { MyArray in
            
            self.QuestionThirtyFourArray = MyArray
            
        }
    }
    
    @IBAction func NumberEight(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "172")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyFourArray, Value: "172") { MyArray in
            
            self.QuestionThirtyFourArray = MyArray
            
        }
    }
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionThirtyFourAnswer", Value: [])
        
        ApiToken.SetItems(Key: "QuestionThirtyFourComment", Value: "")
    }
    
    @IBAction func NextButtonAction(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionThirtyFourAnswer", Value: self.QuestionThirtyFourArray)
        ApiToken.SetItems(Key: "QuestionThirtyFourComment", Value: self.QuestionComment.text)
    }
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirtyFourAnswer", Value: self.QuestionThirtyFourArray)
        ApiToken.SetItems(Key: "QuestionThirtyFourComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
            }
            
        }
    }
    
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer34 = Value
        self.NextButton.isHidden = false
 
        
    }
    
    
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }

}




