//
//  ReminderList.swift
//  Asthma
//
//  Created by MOHAMED on 8/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import Foundation
import SwiftyJSON
class ReminderList {

    var location : String!
    var name : String!
    var alert : String!
    var start : String!
    var reminderRepeat : String!
    
    init(ReminderDict : [String:JSON]) {
        if let Location = ReminderDict["location"]?.string {
            self.location = Location
        }
        
        if let Name = ReminderDict["name"]?.string {
            self.name = Name
        }
        if let Alert = ReminderDict["alert"]?.string {
            self.alert = Alert
        }
        
        if let ReminderRepeat = ReminderDict["repeat"]?.string {
            self.reminderRepeat = ReminderRepeat
        }
        
        if let Start = ReminderDict["start"]?.string {
            self.start = Start
        }
        
        
    }
    
}
