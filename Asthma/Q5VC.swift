//
//  Q5VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/14/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q5VC: UIViewController {

    var QuestionAnswer5 : String!
    
    var QuestionFiveArray = [String]()
    
    @IBOutlet weak var FirstPercentage: UITextField!
    
    
    @IBOutlet weak var SecondPercentage: UITextField!
    
    @IBOutlet weak var ThirdPercentage: UITextField!
    
    @IBOutlet weak var FourthPercentage: UITextField!
    
    @IBOutlet weak var FifthPercentage: UITextField!
    
    var QuestionFourteenMultiArray = Dictionary <String , String>()
    
 
    // Button Atrr
    
    @IBOutlet weak var ButtonOne: UIButton! // 21
    @IBOutlet weak var ButtonTwo: UIButton! //22
    @IBOutlet weak var ButtonThree: UIButton! // 23
    @IBOutlet weak var ButtonFour: UIButton! // 24
    @IBOutlet weak var ButtonFive: UIButton!  // 25
    var TextArray = [String]()
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
        
        ApiMethods.GetQuestionAnswer(Question_id: "5"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for answer in answers! {
                    var obj = answer["answer_id"].string
                    var Percent = answer["percentage"].string
                    self.QuestionFiveArray.append(obj!)

                    let FirstValue = "21"
                    let SeconValue = "22"
                    let ThirdValue = "23"
                    let FourthValue = "24"
                    let FifthValue = "25"
                    
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                        if Percent != nil {
                            self.FirstPercentage.text = Percent
                        }
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        print("My Obj :-",obj)
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                        if Percent != nil {
                            self.SecondPercentage.text = Percent
                        }
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                        if Percent != nil {
                            self.ThirdPercentage.text = Percent
                        }
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                        if Percent != nil {
                            self.FourthPercentage.text = Percent
                        }
                    }
                    
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                        if Percent != nil {
                            self.FifthPercentage.text = Percent
                        }
                     }/*
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionFiveArray)
            
        }
        
        
    }
    
    
    @IBAction func IntialPatient(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "21")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFiveArray, Value: "21") { MyArray in
            
            self.QuestionFiveArray = MyArray
            
        }
    }
    
    @IBAction func ReferalFromHospital(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "22")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFiveArray, Value: "22") { MyArray in
            
            self.QuestionFiveArray = MyArray
            
        }
    }
    
    
    @IBAction func ReferalFromOutSide(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "23")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFiveArray, Value: "23") { MyArray in
            
            self.QuestionFiveArray = MyArray
            
        }

    }
    
    
    @IBAction func NewPatient(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "24")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFiveArray, Value: "24") { MyArray in
            
            self.QuestionFiveArray = MyArray
            
        }
    }
    
    
    @IBAction func Other(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "25")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFiveArray, Value: "25") { MyArray in
            
            self.QuestionFiveArray = MyArray
            
        }
        
    }
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        ApiToken.SetDictionary(Key: "QuestionFivePercentage", Value: [:])

        ApiToken.SetArray(Key: "QuestionFiveAnswer", Value: [])
        ApiToken.SetItems(Key: "QuestionFiveComment", Value: "")
        
    }
    
    @IBAction func NextButtonAction(_ sender: UIButton) {
      //  self.ChecktextField()
        self.ChecktextField(Text: self.FirstPercentage, Sender: ButtonOne, Value: "21")
        self.ChecktextField(Text: self.SecondPercentage, Sender: ButtonTwo, Value: "22")
        self.ChecktextField(Text: self.ThirdPercentage, Sender: ButtonThree, Value: "23")
        self.ChecktextField(Text: self.FourthPercentage, Sender: ButtonFour, Value: "24")
        self.ChecktextField(Text: self.FifthPercentage , Sender: ButtonFive, Value: "25")
        ApiToken.SetDictionary(Key: "QuestionFivePercentage", Value: QuestionFourteenMultiArray)

        print("Five Array :-" , QuestionFourteenMultiArray)

        ApiToken.SetArray(Key: "QuestionFiveAnswer", Value: QuestionFiveArray)
        ApiToken.SetItems(Key: "QuestionFiveComment", Value: self.QuestionComment.text)
        
    }
    func SetItems(value : String , button : UIButton , Value : String) {
        
        ApiToken.SetDictionaryArray(Sender: button, TextValue: value, Value: Value) { (MyArray , key , keyval ) in
            self.QuestionFourteenMultiArray[key] = keyval
            print("Dictionary Array" , MyArray)
        }
    }
    @objc func ChecktextField(Text : UITextField , Sender : UIButton , Value : String) {
        print("Checked")
        if(Text.text?.isEmpty)! {
            self.SetItems(value: "", button: Sender , Value : Value)
            
        }else {
            self.SetItems(value: Text.text!, button: Sender , Value : Value)
            return
        }
        /*
        if(self.SecondPercentage.text?.isEmpty)! {
            self.SetItems(value: "", button: self.ButtonTwo , Value : "22")
        }else {
            self.SetItems(value: self.SecondPercentage.text!, button: self.ButtonTwo , Value : "22")
            return
        }
        
        if(self.ThirdPercentage.text?.isEmpty)!{
            self.SetItems(value: "", button: self.ButtonThree , Value : "23")
            
        }else {
            self.SetItems(value: self.ThirdPercentage.text!, button: self.ButtonThree , Value : "23")
            return
        }
        
        if(self.FourthPercentage.text?.isEmpty)!{
            self.SetItems(value: "", button: self.ButtonFour , Value : "24")
            
        }else{
            self.SetItems(value: self.FourthPercentage.text!, button: self.ButtonFour , Value : "24")
            return
        }
        if(self.FifthPercentage.text?.isEmpty)! {
            self.SetItems(value: "", button: self.ButtonFive , Value : "25")
            
        }else{
            self.SetItems(value: self.FifthPercentage.text!, button: self.ButtonFive , Value : "25")

            return
        }
        */
    }
    /*
    func ChecktextField() {
        guard (self.FirstPercentage.text?.isEmpty)! else {
            self.TextArray.append(self.FirstPercentage.text!)
            return
        }
        guard (self.SecondPercentage.text?.isEmpty)! else {
            self.TextArray.append(self.SecondPercentage.text!)
            return
        }
        guard (self.ThirdPercentage.text?.isEmpty)! else {
            self.TextArray.append(self.ThirdPercentage.text!)
            return
        }
        guard (self.FourthPercentage.text?.isEmpty)! else {
            self.TextArray.append(self.FourthPercentage.text!)
            return
        }
        guard (self.FifthPercentage.text?.isEmpty)! else {
            self.TextArray.append(self.FifthPercentage.text!)
            return
        }
    }
    */
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        self.ChecktextField(Text: self.FirstPercentage, Sender: ButtonOne, Value: "21")
        self.ChecktextField(Text: self.SecondPercentage, Sender: ButtonTwo, Value: "22")
        self.ChecktextField(Text: self.ThirdPercentage, Sender: ButtonThree, Value: "23")
        self.ChecktextField(Text: self.FourthPercentage, Sender: ButtonFour, Value: "24")
        self.ChecktextField(Text: self.FifthPercentage , Sender: ButtonFive, Value: "25")
        ApiToken.SetDictionary(Key: "QuestionFivePercentage", Value: QuestionFourteenMultiArray)

        ApiToken.SetArray(Key: "QuestionFiveAnswer", Value: QuestionFiveArray)
        ApiToken.SetItems(Key: "QuestionFiveComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
            }
            
        }
    }
    
    func QuestionAnswer(Button : UIButton , Value : String)  {
        self.QuestionAnswer5 = Value
        self.NextButton.isHidden = false
        
    }
    
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }

    
}


