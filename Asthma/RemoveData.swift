//
//  RemoveData.swift
//  Asthma
//
//  Created by MOHAMED on 11/9/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import Foundation
class RemoveData {
    class func removeDataObj () {
        UserDefaults.standard.removeObject(forKey: "QuestionOneAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionOneComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionTwoAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionTwoComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionThreeAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionThreeComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionFourAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionFourComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionFiveAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionFiveComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionSexAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionSexComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionSevenAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionSevenComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionEightAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionEightComment")
        
        
        UserDefaults.standard.removeObject(forKey: "QuestionNineAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionNineComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionTenAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionTenComment")
        
        
        UserDefaults.standard.removeObject(forKey: "QuestionElevenAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionElevenComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionTwelveAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionTwelveComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionThirteenAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionThirteenComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionFourteenAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionFourteenComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionFifteenUpdatedAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionFifteenUpdatedComment")
        
        UserDefaults.standard.removeObject(forKey: "QuiestionSixteenUpdatedAnswer")
        UserDefaults.standard.removeObject(forKey: "QuiestionSixteenUpdatedComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionFifteenAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionFifteenComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionSexteenAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionSexteenComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionSeventeenAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionSeventeenComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionEighteenAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionEighteenComment")
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyUpdatedAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyUpdatedComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionNinteentAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionNinteenComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyOneAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyOneComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyTwoAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyTwoComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyThreeAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyThreeComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyFourAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyFourComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyFiveAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyFiveComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionTwentySexAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionTwentySexComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionTwentySevenAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionTwentySevenComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyEightAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyEightComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyNineAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionTwentyNineComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyOneAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyOneComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyTwoAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyTwoComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyThreeAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyThreeComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyFourAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyFourComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyFiveAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyFiveComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionThirtySexAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionThirtySexComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionThirtySevenAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionThirtySevenComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyEightAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyEightComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyNineAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionThirtyNineComment")
        
        UserDefaults.standard.removeObject(forKey: "QuestionFourtyAnswer")
        UserDefaults.standard.removeObject(forKey: "QuestionFourtyComment")
        UserDefaults.standard.removeObject(forKey: "QuestionFivePercentage")
        UserDefaults.standard.removeObject(forKey: "QuestionElevenPercentage")
      
        
        UserDefaults.standard.synchronize()
    }
}
