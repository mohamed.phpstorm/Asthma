//
//  ViewController.swift
//  Asthma
//
//  Created by MOHAMED on 5/21/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import JTAppleCalendar
class ViewController: UIViewController {
    @IBOutlet weak var UserNameText: UILabel!
    
    @IBOutlet weak var lastLoginText: UILabel!
    @IBOutlet weak var calenderView: JTAppleCalendarView!
    @IBOutlet weak var YearText: UILabel!
    
    @IBOutlet weak var UserImage: UIButton!
    @IBOutlet weak var MonthText: UILabel!
    let formatter = DateFormatter()
    var datesArray = [Date]()
    var repeatArray = [Date]()
    override func viewDidLoad() {
        super.viewDidLoad()
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.GetMedicalRepData(api_token: api_token){ (error , last_login , name , photo) in
                if error == nil {
                    self.lastLoginText.text = last_login
                    self.UserNameText.text = name
                    if photo != nil {
                        let ImageUrl = URL(string: ImageRoot + photo!)!
                        self.UserImage.kf.setImage(with: ImageUrl, for: .normal)
                    }else {
                        self.UserImage.setImage(#imageLiteral(resourceName: "Avatar"), for: .normal)
                    }                }
            }
        }
        self.YearText.isHidden = true
        self.calenderView.layer.cornerRadius = 20.0
        // Do any additional setup after loading the view, typically from a nib.
        setupCalenderView()
        
        loadReminders()
    }
    @IBAction func MenuBuAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuVC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    @IBAction func NotificationAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SchedualScreen") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func LogoutAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutScreen") as! LogoutVC
        self.present(vc, animated: true, completion: nil)
    }
    
    func setupCalenderView() {
        calenderView.visibleDates{ (visibleDates) in
            self.setupViewedCalender(from: visibleDates)
        }
    }
    
    func setupViewedCalender( from  visibleDates: DateSegmentInfo){
        let date = visibleDates.monthDates.first?.date
        self.formatter.dateStyle = .long
        self.formatter.dateFormat = "MMM - yyyy"
        self.MonthText.text = self.formatter.string(from: date!)
        let previousMonth = Calendar.current.date(byAdding: .month, value: -1, to: Date())
        let nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: Date())
        
    }
    
}

extension ViewController :  JTAppleCalendarViewDataSource {
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "YYYY MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        let startStringDate = formatter.string(from: Date())
        let startDate = formatter.date(from: startStringDate)!
        print("My Start Date  :- " , startDate)
        let systemCurrentDate = Calendar.current
        let EndDate = systemCurrentDate.date(byAdding: .year, value: 10, to: Date())
        let endStringDate = formatter.string(from: EndDate!)
        let  endDate = formatter.date(from: endStringDate)!
        print("My End Date :- " ,endDate)
        
        let parameters =  ConfigurationParameters(startDate: startDate, endDate: endDate)
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewedCalender(from: visibleDates)
    }
    
    
}

extension ViewController : JTAppleCalendarViewDelegate {
    
    func loadReminders() {
        if let api_token = ApiToken.getApiToken() {
            
            ApiMethods.GetReminders(api_token: api_token){(error , secondDate) in
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "YYYY MM dd" //Your date format
                //  dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
                
                if secondDate != nil {
                    
                    print(secondDate)
                    
                    for obj in secondDate! {
                        if  let obj = obj["start"].string {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            //   dateFormatter.locale = Locale.init(identifier: "en_GB")
                            
                            let dateObj = dateFormatter.date(from: obj)
                            
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            let dateString =  dateFormatter.string(from: dateObj!)
                            //var cellString = dateFormatter.string(from: cellState.date)
                            
                            let MyDate = dateFormatter.date(from: dateString)
                            //var cellDate = dateFormatter.date(from: cellString)
                            
                            self.datesArray.append(MyDate!)
                            
                            
                        }
                        
                    }
                    for secondObj in secondDate! {
                        if  let secondObj = secondObj["repeat"].string {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            //   dateFormatter.locale = Locale.init(identifier: "en_GB")
                            
                            let dateObj = dateFormatter.date(from: secondObj)
                            
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            let dateString =  dateFormatter.string(from: dateObj!)
                            //var cellString = dateFormatter.string(from: cellState.date)
                            
                            let MyDate = dateFormatter.date(from: dateString)
                            //var cellDate = dateFormatter.date(from: cellString)
                            
                            self.repeatArray.append(MyDate!)
                            
                            
                        }
                        
                    }
                    
                    self.calenderView.reloadData()
                }
            }
            
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CalenderCell", for: indexPath) as! CalenderCell
        cell.dateLabel.text = cellState.text
        
        if let firstDate = self.repeatArray.filter({
            return $0 == cellState.date
        }).first {
            
            // found matched date
            cell.backgroundColor = UIColor(red: 228/255, green: 151/255, blue: 44/255, alpha: 1)
            cell.dateLabel.textColor = UIColor.white
            
        }else if let secondDate = self.datesArray.filter({
            return $0 == cellState.date
        }).first {
            
            // found matched date
            cell.backgroundColor = UIColor(red: 52/255, green: 203/255, blue: 158/255, alpha: 1)
            cell.dateLabel.textColor = UIColor.white

            
        }else {
            cell.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
            cell.dateLabel.textColor = UIColor.black

        }
        
        
        /*print("My Array" , datesArray)
        for coloredDate in datesArray {
            
            
            if  coloredDate == cellState.date{
                cell.backgroundColor = UIColor(red: 199/255, green: 217/255, blue: 231/255, alpha: 1)
                
                
                print("Colored")
            }
            
            
            
        }*/
        //cell.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        return cell
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        let date = cellState.date
        print(date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
         let datestring = dateFormatter.string(from : date)
         ApiToken.SetItems(Key: "UserDate", Value: datestring)
        performSegue(withIdentifier: "GetReminderByUser", sender: self)
        
        // print("date :- " dat)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    
}


