//
//  ResetPasswordVC.swift
//  Asthma
//
//  Created by MOHAMED on 7/11/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit

class ResetPasswordVC: UITableViewController {

    @IBOutlet weak var EmailText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.CustomizeTextField(Field: EmailText)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    func CustomizeTextField(Field:UITextField){
        Field.backgroundColor = UIColor.black
        Field.layer.cornerRadius = 20.0
        let mycolor : UIColor = UIColor.white
        Field.layer.borderColor = mycolor.cgColor
        Field.layer.borderWidth = 2
        
    }
    
    @IBAction func ResetPasswordAction(_ sender: UIButton) {
        guard let email = self.EmailText.text , !email.isEmpty else {
            
            let message = "You Need To Fil Email Field"
            let title  = "Error"
            self.AlertAction(message: message,title: title)
            return
        }
        ApiMethods.MedicalRepResetPassword(email: EmailText.text!) {(status , message , error) in
            if status == 1 {
                let message = "Please Check Your Email"
                let title = "Success"
                self.AlertAction(message: message, title: title)
            }
            
        }
        
    }
    
    func AlertAction(message : String, title : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
}
