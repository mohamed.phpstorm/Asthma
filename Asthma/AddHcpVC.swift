//
//  AddHcpVC.swift
//  Asthma
//
//  Created by MOHAMED on 6/6/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
class AddHcpVC: UIViewController {
    
    
    @IBOutlet weak var DoctorName: UITextField!
    
    @IBOutlet weak var Specialist: UIButton!
    
    
    @IBOutlet weak var AuthorityText: UITextField!
    
    
    @IBOutlet weak var Speciality: UITextField!
    
    @IBOutlet weak var HospitalText: UITextField!
    
    @IBOutlet weak var SectorText: UIButton!
    
    @IBOutlet weak var Country: UIButton!
    
    @IBOutlet weak var City: UIButton!
    
    
    @IBOutlet weak var UserImage: UIButton!
    @IBOutlet weak var LastLogintext: UILabel!
    @IBOutlet weak var UserNameText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.GetMedicalRepData(api_token: api_token){ (error , last_login , name , photo) in
                if error == nil {
                    self.LastLogintext.text = last_login
                    self.UserNameText.text = name
                    if photo != nil {
                        let ImageUrl = URL(string: ImageRoot + photo!)!
                        self.UserImage.kf.setImage(with: ImageUrl, for: .normal)
                    }else {
                        self.UserImage.setImage(#imageLiteral(resourceName: "Avatar"), for: .normal)
                    }                }
            }
        }
        
        // Do any additional setup after loading the view.
    }
    @IBAction func SpecialistActionBu(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        //'Consultant','Specialist','Head Of Department'
        let Consultant = "Consultant"
        let Specialist = "Specialist"
        let Head = "Head Of Department"
        
        alert.addAction(UIAlertAction.init(title: Consultant, style: .default, handler: { (action) in
            sender.setTitle(Consultant, for: .normal)
        }))
        
        alert.addAction(UIAlertAction.init(title: Specialist, style: .default, handler: { (action) in
            sender.setTitle(Specialist, for: .normal)
            
            
        }))
        alert.addAction(UIAlertAction.init(title: Head, style: .default, handler: { (action) in
            sender.setTitle(Head, for: .normal)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    @IBAction func SectorActionBu(_ sender: UIButton) {
        //'Private',  'INST',  'semi INST'
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        let Private = "Private"
        let INST = "INST"
        let Semi = "semi INST"
        
        alert.addAction(UIAlertAction.init(title: Private, style: .default, handler: { (action) in
            sender.setTitle(Private, for: .normal)
        }))
        
        alert.addAction(UIAlertAction.init(title: INST, style: .default, handler: { (action) in
            sender.setTitle(INST, for: .normal)
        }))
        alert.addAction(UIAlertAction.init(title: Semi, style: .default, handler: { (action) in
            sender.setTitle(Semi, for: .normal)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func CountryActionBu(_ sender: UIButton) {
        ApiMethods.GetCountrie(){ (error , CountryData) in
            print("Api Methods")
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
            if CountryData != nil {
                for obj in CountryData! {
                    print("OBJ :-" , obj)
                    alert.addAction(UIAlertAction.init(title: obj, style: .default, handler: { (action) in
                        sender.setTitle(obj, for: .normal)
                    }))
                    
                }
                alert.addAction(UIAlertAction(title: "Canel", style: UIAlertActionStyle.cancel, handler:nil))
                self.present(alert, animated: true, completion: nil)
                
                
            }
        }
    }
    
    
    
    @IBAction func CityActionBu(_ sender: UIButton) {
        if self.Country.titleLabel?.text != "Country" {
            ApiMethods.GetCities(Name: self.Country.titleLabel?.text){ (error , CitiesData) in
                print("My Func")
                
                let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
                if CitiesData != nil {
                    for obj in CitiesData! {
                        print("Obj City" , obj)
                        alert.addAction(UIAlertAction.init(title: obj, style: .default, handler: { (action) in
                            sender.setTitle(obj, for: .normal)
                        }))
                        
                    }
                    alert.addAction(UIAlertAction(title: "Canel", style: UIAlertActionStyle.cancel, handler:nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
        }else {
            let message = "You Need To Choose Country First"
            let title  = "Error"
            self.AlertAction(message: message,title: title)
        }
        
        
    }
    @IBAction func AddDoctor(_ sender: UIButton) {
        guard let doctortitle = DoctorName.text , !doctortitle.isEmpty else {
            let message = "You Need To Fil Title Field"
            let title  = "Error"
            self.AlertAction(message: message,title: title)
            return
        }
        
        guard let doctorname = DoctorName.text , !doctorname.isEmpty else {
            let message = "You Need To Fil Name Field"
            let title  = "Error"
            self.AlertAction(message: message,title: title)
            
            return
        }
        
        guard let doctorspe = Speciality.text , !doctorspe.isEmpty else {
            let message = "You Need To Fil Specialty Field"
            let title  = "Error"
            self.AlertAction(message: message,title: title)
            return
        }
        if let api_token = ApiToken.getApiToken(){
            
            ApiMethods.AddHcpDoctor(api_token: api_token, name: self.DoctorName.text, specialty: self.Speciality.text, specialist_consultant: self.Specialist.titleLabel?.text, city: self.City.titleLabel?.text, country: self.Country.titleLabel?.text, authority: self.AuthorityText.text, hospital: self.HospitalText.text, sector: self.SectorText.titleLabel?.text){(error , status) in
                if status == 0 {
                    let message = "UnKnown Error Please Try Again"
                    let title  = "Error"
                    self.AlertAction(message: message,title: title)
                }else {
                    
                    let message = "Hcp Added Succefully"
                    let title  = "congratulations"
                    self.AlertAction(message: message , title: title)
                    
                }
                
            }
            
        }
        
    }
    
    func AlertAction(message : String, title : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func MenuBuAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuVC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    @IBAction func NotificationAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SchedualScreen") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func LogoutAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutScreen") as! LogoutVC
        self.present(vc, animated: true, completion: nil)
    }
    
}
