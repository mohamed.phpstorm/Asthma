//
//  SeverAsthma.swift
//  Asthma
//
//  Created by MOHAMED on 6/5/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit

class SeverAsthma: UIViewController , UITableViewDelegate , UITableViewDataSource {

    
    @IBOutlet weak var LastLoginText: UILabel!
    
    @IBOutlet weak var UserNameText: UILabel!
    
    @IBOutlet weak var UserImage: UIButton!
    
    var Sever = [SeverAsthmaModel]()
    lazy var refresher : UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(handelRefresh), for: .valueChanged)
        return refresher
    }()
    var current_page :Int = 1
    var last_page : Int = 1
    var isLoading : Bool = false
    
    var MenuIsOpen = false
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.GetMedicalRepData(api_token: api_token){ (error , last_login , name , photo) in
                if error == nil {
                    self.LastLoginText.text = last_login
                    self.UserNameText.text = name
                    if photo != nil {
                        let ImageUrl = URL(string: ImageRoot + photo!)!
                        self.UserImage.kf.setImage(with: ImageUrl, for: .normal)
                    }else {
                        self.UserImage.setImage(#imageLiteral(resourceName: "Avatar"), for: .normal)
                    }
                }
            }
        }
        self.tableView.layer.masksToBounds = true
        self.tableView.layer.borderColor = UIColor(red: 90/255, green: 166/255, blue: 157/255, alpha: 1).cgColor
        self.tableView.layer.borderWidth = 3.0
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.addSubview(refresher)
        
        handelRefresh()
        
    }
    
    @objc private func handelRefresh() {
        
        self.refresher.endRefreshing()
        
        guard !isLoading else {return}
        
        isLoading = true
        if let api_token = ApiToken.getApiToken(){
            ApiMethods.GetAllArticels(api_token: api_token){ (error : Error?, articles :[SeverAsthmaModel]?, last_page : Int) in
                print(api_token)
                self.isLoading = false
                
                if let articles = articles {
                    
                    self.Sever = articles
                    
                    self.tableView.reloadData()
                    
                    self.current_page = 1
                    
                    self.last_page = last_page
                    
                }
                
            }
            
            
        }
    }
    func loadMore ()  {
        
        guard !isLoading else {return}
        
        guard current_page < last_page else {return}
        
        isLoading = true
        if let api_token = ApiToken.getApiToken(){
            ApiMethods.GetAllArticels(api_token: api_token){ (error : Error?, articles :[SeverAsthmaModel]?, last_page : Int) in
                
                self.isLoading = false
                
                if let articles = articles {
                    
                    self.Sever.append(contentsOf: articles)
                    
                    self.tableView.reloadData()
                    
                    
                    self.current_page += 1
                    
                    self.last_page = last_page
                    
                }
                
            }
        }
        
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Sever.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SeverCell") as! SeverCell
        
        let article = Sever[indexPath.row]
        cell.ConfigureCell(Article: article)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        let currentCell = tableView.cellForRow(at: indexPath!)! as! SeverCell
        if let api_token = ApiToken.getApiToken() {
        if let url = URL(string: PdfArtilcUrl + currentCell.FileAttach.text!){
            print(url)
            UIApplication.shared.openURL(url)
        }
        }
    }
    
    @IBAction func MenuBuAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuVC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    @IBAction func NotificationAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SchedualScreen") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func LogoutAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutScreen") as! LogoutVC
        self.present(vc, animated: true, completion: nil)
    }

    
    
}
