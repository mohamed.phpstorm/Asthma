//
//  EditHcpVC.swift
//  Asthma
//
//  Created by MOHAMED on 6/16/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class EditHcpVC: UIViewController {
    
    @IBOutlet weak var UserNameText: UILabel!
    @IBOutlet weak var LastLoginText: UILabel!
    var HCpId : String!
    @IBOutlet weak var UserImage: UIButton!
    
    @IBOutlet weak var DoctorName: UITextField!
    
    @IBOutlet weak var Specialist: UIButton!
    
    
    @IBOutlet weak var AuthorityText: UITextField!
    
    
    @IBOutlet weak var Speciality: UITextField!
    
    @IBOutlet weak var HospitalText: UITextField!
    
    @IBOutlet weak var SectorText: UIButton!
    
    @IBOutlet weak var Country: UIButton!
    
    @IBOutlet weak var City: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SetItems(id: HCpId)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.GetMedicalRepData(api_token: api_token){ (error , last_login , name , photo) in
                if error == nil {
                    self.LastLoginText.text = last_login
                    self.UserNameText.text = name
                    if photo != nil {
                        let ImageUrl = URL(string: ImageRoot + photo!)!
                        self.UserImage.kf.setImage(with: ImageUrl, for: .normal)
                    }else {
                        self.UserImage.setImage(#imageLiteral(resourceName: "Avatar"), for: .normal)
                    }                }
            }
        }
        
        // Do any additional setup after loading the view.
    }
    func SetItems(id : String) {
        if let api_token = ApiToken.getApiToken() {
            let parameters = [
                "api_token" : api_token
            ]
            let url = URL(string: DoctorDetails + id)!
            
            Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
                
                switch response.result {
                    
                case .failure(let error) :
                    print(error)
                case .success(let value):
                    print(value)
                    var json = JSON(value)
                    
                    let data = json["data"].dictionary
                    let name = data?["name"]?.string
                    let specialty = data?["specialty"]?.string
                    let specialist_consultant = data?["specialist_consultant"]?.string
                    let authority = data?["authority"]?.string
                    let hospital = data?["hospital"]?.string
                    let sector = data?["sector"]?.string
                    if let city = data?["city_id"]?.dictionary {
                        let CityString = city["name"]?.string
                        self.City.setTitle(CityString, for: .normal)
                    }
                    if let country = data?["country_id"]?.dictionary {
                        let CountryString = country["name"]?.string
                        self.Country.setTitle(CountryString, for: .normal)

                    }
                  
                    self.DoctorName.text = name
                    self.Speciality.text = specialty
                    self.Specialist.titleLabel?.text = specialist_consultant
                    self.SectorText.setTitle(sector, for: .normal)
                    self.AuthorityText.text = authority
                    self.HospitalText.text = hospital
                    
                    
                }
            }
            
        }
    }
    @IBAction func SpecialistActionBu(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        //'Consultant','Specialist','Head Of Department'
        let Consultant = "Consultant"
        let Specialist = "Specialist"
        let Head = "Head Of Department"
        
        alert.addAction(UIAlertAction.init(title: Consultant, style: .default, handler: { (action) in
            sender.setTitle(Consultant, for: .normal)
        }))
        
        alert.addAction(UIAlertAction.init(title: Specialist, style: .default, handler: { (action) in
            sender.setTitle(Specialist, for: .normal)
            
            
        }))
        alert.addAction(UIAlertAction.init(title: Head, style: .default, handler: { (action) in
            sender.setTitle(Head, for: .normal)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    @IBAction func SectorActionBu(_ sender: UIButton) {
        //'Private',  'INST',  'semi INST'
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        let Private = "Private"
        let INST = "INST"
        let Semi = "semi INST"
        
        alert.addAction(UIAlertAction.init(title: Private, style: .default, handler: { (action) in
            sender.setTitle(Private, for: .normal)
        }))
        
        alert.addAction(UIAlertAction.init(title: INST, style: .default, handler: { (action) in
            sender.setTitle(INST, for: .normal)
        }))
        alert.addAction(UIAlertAction.init(title: Semi, style: .default, handler: { (action) in
            sender.setTitle(Semi, for: .normal)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func CountryActionBu(_ sender: UIButton) {
        ApiMethods.GetCountrie(){ (error , CountryData) in
            print("Api Methods")
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
            if CountryData != nil {
                for obj in CountryData! {
                    print("OBJ :-" , obj)
                    alert.addAction(UIAlertAction.init(title: obj, style: .default, handler: { (action) in
                        sender.setTitle(obj, for: .normal)
                    }))
                    
                }
                alert.addAction(UIAlertAction(title: "Canel", style: UIAlertActionStyle.cancel, handler:nil))
                self.present(alert, animated: true, completion: nil)
                
                
            }
        }
    }
    
    
    
    @IBAction func CityActionBu(_ sender: UIButton) {
        if self.Country.titleLabel?.text != "Country" {
            ApiMethods.GetCities(Name: self.Country.titleLabel?.text){ (error , CitiesData) in
                print("My Func")
                
                let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
                if CitiesData != nil {
                    for obj in CitiesData! {
                        print("Obj City" , obj)
                        alert.addAction(UIAlertAction.init(title: obj, style: .default, handler: { (action) in
                            sender.setTitle(obj, for: .normal)
                        }))
                        
                    }
                    alert.addAction(UIAlertAction(title: "Canel", style: UIAlertActionStyle.cancel, handler:nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
        }else {
            let message = "You Need To Choose Country First"
            let title  = "Error"
            self.AlertAction(message: message,title: title)
        }
        
        
    }
    
    
    @IBAction func AddDoctor(_ sender: UIButton) {
        
      
        guard let doctorname = DoctorName.text , !doctorname.isEmpty else {
            let message = "You Need To Fil Name Field"
            let title  = "Error"
            self.AlertAction(message: message,title: title)
            
            return
        }
        
        guard let doctorspe = Speciality.text , !doctorspe.isEmpty else {
            let message = "You Need To Fil Specialty Field"
            let title  = "Error"
            self.AlertAction(message: message,title: title)
            return
        }
        if let api_token = ApiToken.getApiToken(){
            ApiMethods.EditHcpDoctor(api_token: api_token, id: self.HCpId, name: self.DoctorName.text, specialty: self.Speciality.text, specialist_consultant: self.Specialist.titleLabel?.text, city: self.City.titleLabel?.text, country: self.Country.titleLabel?.text, authority: self.AuthorityText.text, hospital: self.HospitalText.text, sector: self.SectorText.titleLabel?.text){(error , status) in
                if status == 0 {
                    let message = "UnKnown Error Please Try Again"
                    let title  = "Error"
                    self.AlertAction(message: message,title: title)
                }else {
                    let message = "Hcp Edited Succefully"
                    let title  = "congratulations"
                    self.AlertAction(message: message , title: title)
                    
                }
                
            }
            
        }
    }
    
    
    @IBAction func DeleteHcpDoctor(_ sender: UIButton) {
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.DeleteHcpDoctor(api_token: api_token, id: self.HCpId, compltion: { (error, status) in
                if status == 1 {
                    ApiToken.restartApp()
                }
                
            })
        }
        
    }
    
    
    
    
    
    func AlertAction(message : String, title : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func MenuBuAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuVC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    @IBAction func NotificationAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SchedualScreen") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func LogoutAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutScreen") as! LogoutVC
        self.present(vc, animated: true, completion: nil)
    }
    
}
