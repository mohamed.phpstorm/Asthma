
//
//  RegisterVC.swift
//  Asthma
//
//  Created by MOHAMED on 5/29/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit

class RegisterVC: UITableViewController {

    @IBOutlet weak var UserNameText: UITextField!
    @IBOutlet weak var ConfirmPasswordText: UITextField!
    @IBOutlet weak var PasswordText: UITextField!
    @IBOutlet weak var EmailText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.CustomizeTextField(Field: ConfirmPasswordText)
        self.CustomizeTextField(Field: PasswordText)
        self.CustomizeTextField(Field: EmailText)
        self.CustomizeTextField(Field: UserNameText)

    }
    func CustomizeTextField(Field:UITextField){
        Field.backgroundColor = UIColor.black
        Field.layer.cornerRadius = 20.0
        let mycolor : UIColor = UIColor.white
        Field.layer.borderColor = mycolor.cgColor
        Field.layer.borderWidth = 2
        
    }

    @IBAction func RegisterBuAction(_ sender: UIButton) {
        guard let username = self.UserNameText.text , !username.isEmpty else {
            
            let message = "You Need To Fill UserName Field"
            let title  = "Error"
            self.AlertAction(message: message,title: title)
            return
        }
        
        guard let email = self.EmailText.text , !email.isEmpty else {
            
            let message = "You Need To Fill Email Field"
            let title  = "Error"
            self.AlertAction(message: message,title: title)
            return
        }
        
        guard let password = self.PasswordText.text , !password.isEmpty  else {
            let message = "You Need To Fill Password Field"
            let title  = "Error"
            self.AlertAction(message: message,title: title)
            return
        }
        
        guard let confirmPassword = self.ConfirmPasswordText.text , !confirmPassword.isEmpty  else {
            let message = "You Need To Confirm Password "
            let title  = "Error"
            self.AlertAction(message: message,title: title)
            return
        }
        
        ApiMethods.MedicalRepRegister(name: self.UserNameText.text!, password: self.PasswordText.text!, email: self.EmailText.text!, password_confirmation: self.ConfirmPasswordText.text!) {(status , message , error) in
            if (status == 1) {
                let title = "Congratulations"
                let message  = "Registered Succefully"
                self.AlertAction(message: message, title: title)
            }else {
                let message = "Invalid Username or Password"
                let title  = "Error"
                self.AlertAction(message: message, title: title)
            }
            
        }
    }
    
    func AlertAction(message : String, title : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
    }

  
}
