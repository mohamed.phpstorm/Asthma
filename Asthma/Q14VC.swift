//
//  Q14VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
class Q14VC: UIViewController {
    
    
    var QuestionAnswer14 : String!
    
    var QuestionFourteenArray = [String]()
    var QuestionFourteenMultiArray = [Dictionary<String, [String]>]()
    
    @IBOutlet weak var LabelOneText: UILabel!
    
    @IBOutlet weak var LabelTwoText: UILabel!
    
    @IBOutlet weak var LabelThreeText: UILabel!
    
    @IBOutlet weak var LabelFourText: UILabel!
    
    
    @IBOutlet weak var LabelFiveText: UILabel!
    
    
    @IBOutlet weak var LabelSexText: UILabel!
    
    
    @IBOutlet weak var LabelSevenText: UILabel!
    
    
    @IBOutlet weak var LabelEightText: UILabel!
    // Button Atrr
    @IBOutlet weak var LabelNineText: UILabel!
    
    @IBOutlet weak var ButtonOne: UIButton! // 63
    
    @IBOutlet weak var ButtonTwo: UIButton! //64
    
    @IBOutlet weak var ButtonThree: UIButton! //65
    
    @IBOutlet weak var ButtonFour: UIButton!  //66
    
    @IBOutlet weak var ButtonFive: UIButton!  //67
    
    
    @IBOutlet weak var ButtonSex: UIButton!  //68
    
    
    @IBOutlet weak var ButtonSeven: UIButton! //69
    
    
    @IBOutlet weak var ButtonEight: UIButton!  //70
    
    
    @IBOutlet weak var ButtonNine: UIButton!
    
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
        print("Screen 14")

        ApiMethods.GetQuestionAnswer(Question_id: "13"){(answers , error , comment) in
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    if let obj = obj["answer"].array {
                        var firstarray = obj[0][0]
                        var seconarray = obj[1][0]
                       // print("Secon Va :-" , seconarray)
                        var getanswer = firstarray["get_answer"]
                        var firstId = getanswer["id"].int
                        print("First Id :- " , getanswer)
                        var parentanswer = seconarray["parent_answer"]
                        var secondId = parentanswer["id"].int
                        
                        var FirstValue = "63"
                        let SecondValue = "64"
                        var ThirdValue = "65"
                        var FourthValue = "66"
                        var FifthValue = "67"
                        var SexValue = "68"
                        var SevnthValue = "69"
                        var EightValue = "70"
                        // ButtonOne
                        if  String(firstId!) == FirstValue as? String {
                            ApiToken.ChangeCheckBoxImage(CheckBox: self.ButtonOne)

                            ApiToken.SetArrayQuestionFourteen(Sender: self.ButtonOne, TextValue: String(describing: secondId!), QuestionArray:  self.QuestionFourteenMultiArray, Value: FirstValue) { MyArray in
                                
                                self.QuestionFourteenMultiArray = MyArray
                                print("Fourteen Array :-" , MyArray)
                                
                            }
                        }
                        // Button Two
                        if  String(firstId!) == SecondValue  as? String {
                            ApiToken.ChangeCheckBoxImage(CheckBox: self.ButtonTwo)

                            ApiToken.SetArrayQuestionFourteen(Sender: self.ButtonTwo , TextValue: String(describing: secondId!), QuestionArray:  self.QuestionFourteenMultiArray, Value: SecondValue) { MyArray in
                                
                                self.QuestionFourteenMultiArray = MyArray
                                print("Fourteen Array :-" , MyArray)
                                
                            }
                        }
                        // Button Three
                        if  String(firstId!) == ThirdValue  as? String {
                            ApiToken.ChangeCheckBoxImage(CheckBox: self.ButtonThree)

                            ApiToken.SetArrayQuestionFourteen(Sender: self.ButtonThree , TextValue: String(describing: secondId!), QuestionArray:  self.QuestionFourteenMultiArray, Value: ThirdValue) { MyArray in
                                
                                self.QuestionFourteenMultiArray = MyArray
                                print("Fourteen Array :-" , MyArray)
                                
                            }                        }
                        // Button Four
                        if  String(firstId!) == FourthValue  as? String {
                            ApiToken.ChangeCheckBoxImage(CheckBox: self.ButtonFour)

                            ApiToken.SetArrayQuestionFourteen(Sender: self.ButtonFour , TextValue: String(describing: secondId!), QuestionArray:  self.QuestionFourteenMultiArray, Value: FourthValue) { MyArray in
                                
                                self.QuestionFourteenMultiArray = MyArray
                                print("Fourteen Array :-" , MyArray)
                                
                            }
                        }
                        
                        // Button Five
                        if  String(firstId!) == FifthValue  as? String {
                            ApiToken.ChangeCheckBoxImage(CheckBox: self.ButtonFive)

                            ApiToken.SetArrayQuestionFourteen(Sender: self.ButtonFive , TextValue: String(describing: secondId!), QuestionArray:  self.QuestionFourteenMultiArray, Value: FifthValue) { MyArray in
                                
                                self.QuestionFourteenMultiArray = MyArray
                                print("Fourteen Array :-" , MyArray)
                                
                            }
                        }
                        // Button Sex
                        if  String(firstId!) == SexValue  as? String {
                            ApiToken.ChangeCheckBoxImage(CheckBox: self.ButtonSex)

                            ApiToken.SetArrayQuestionFourteen(Sender: self.ButtonSex , TextValue: String(describing: secondId!), QuestionArray:  self.QuestionFourteenMultiArray, Value: SexValue) { MyArray in
                                
                                self.QuestionFourteenMultiArray = MyArray
                                print("Fourteen Array :-" , MyArray)
                                
                            }
                        }
                        // Button Seven
                        if  String(firstId!) == SevnthValue  as? String {
                            ApiToken.ChangeCheckBoxImage(CheckBox: self.ButtonSeven)

                            ApiToken.SetArrayQuestionFourteen(Sender: self.ButtonSeven , TextValue: String(describing: secondId!), QuestionArray:  self.QuestionFourteenMultiArray, Value: SevnthValue) { MyArray in
                                
                                self.QuestionFourteenMultiArray = MyArray
                                print("Fourteen Array :-" , MyArray)
                                
                            }
                        }
                        if  String(firstId!) == EightValue  as? String {
                            ApiToken.ChangeCheckBoxImage(CheckBox: self.ButtonEight)
                            
                            ApiToken.SetArrayQuestionFourteen(Sender: self.ButtonEight , TextValue: String(describing: secondId!), QuestionArray:  self.QuestionFourteenMultiArray, Value: EightValue) { MyArray in
                                
                                self.QuestionFourteenMultiArray = MyArray
                                
                                print("Fourteen Array :-" , MyArray)
                                
                            }
                        }
                        
                        
                        
                    }
                    
                }
            }
            
        }
        
    }
    @IBAction func NumberOne(_ sender: DLRadioButton) {
        SetItemsLabel(label: LabelOneText , Value : "63" , Sender : sender)
        QuestionAnswer(Button : sender , Value : "63")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        
    }
    
    @IBAction func NumberTwo(_ sender: DLRadioButton) {
        SetItemsLabel(label: LabelTwoText,  Value : "64" , Sender : sender)
        QuestionAnswer(Button : sender , Value : "64")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        
    }
    
    @IBAction func NumberThree(_ sender: DLRadioButton) {
        SetItemsLabel(label: LabelThreeText , Value : "65" , Sender : sender)
        QuestionAnswer(Button : sender , Value : "65")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
    }
    
    
    @IBAction func NumberFour(_ sender: DLRadioButton) {
        SetItemsLabel(label: LabelFourText , Value : "66" , Sender : sender)
        QuestionAnswer(Button : sender , Value : "66")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
    }
    
    @IBAction func NumberFive(_ sender: DLRadioButton) {
        SetItemsLabel(label: LabelFiveText , Value : "67" , Sender : sender)
        QuestionAnswer(Button : sender , Value : "67")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
    }
    
    @IBAction func NumberSex(_ sender: DLRadioButton) {
        SetItemsLabel(label: LabelSexText , Value : "68" , Sender : sender)
        QuestionAnswer(Button : sender , Value : "68")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        
    }
    
    @IBAction func NumberSeven(_ sender: DLRadioButton) {
        SetItemsLabel(label: LabelSevenText , Value : "69" , Sender : sender)
        QuestionAnswer(Button : sender , Value :"69")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        
    }
    
    
    @IBAction func NumberEight(_ sender: DLRadioButton) {
        SetItemsLabel(label: LabelEightText ,Value : "70" , Sender : sender)
        QuestionAnswer(Button : sender , Value : "70")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
    }
    
    @IBAction func NumberNine(_ sender: UIButton) {
        SetItemsLabel(label: LabelNineText ,Value : "227" , Sender : sender)
      //  QuestionAnswer(Button : sender , Value : "70")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
    }
    func SetItemsLabel(label : UILabel, Value : String , Sender : UIButton){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        let first = "Once per year"
        let second = "At each follow up visit"
        let third = "Every 3 months"
        let fourth = "Others"
        
        alert.addAction(UIAlertAction.init(title: first, style: .default, handler: { (action) in
            label.text = first
            ApiToken.SetArrayQuestionFourteen(Sender: Sender, TextValue: "199", QuestionArray:  self.QuestionFourteenMultiArray, Value: Value) { MyArray in
                
                self.QuestionFourteenMultiArray = MyArray
                print("Fourteen Array :-" , MyArray)
                
            }
        }))
        
        alert.addAction(UIAlertAction.init(title: second, style: .default, handler: { (action) in
            label.text = second
            ApiToken.SetArrayQuestionFourteen(Sender: Sender, TextValue: "200", QuestionArray:  self.QuestionFourteenMultiArray, Value: Value) { MyArray in
                
                self.QuestionFourteenMultiArray = MyArray
                print("Fourteen Array :-" , MyArray)
                
            }
        }))
        
        alert.addAction(UIAlertAction.init(title: third, style: .default, handler: { (action) in
            label.text = third
            ApiToken.SetArrayQuestionFourteen(Sender: Sender, TextValue: "201", QuestionArray:  self.QuestionFourteenMultiArray, Value: Value) { MyArray in
                
                self.QuestionFourteenMultiArray = MyArray
                print("Fourteen Array :-" , MyArray)
                
            }
        }))
        alert.addAction(UIAlertAction.init(title: fourth, style: .default, handler: { (action) in
            label.text = fourth
            ApiToken.SetArrayQuestionFourteen(Sender: Sender, TextValue: "202", QuestionArray:  self.QuestionFourteenMultiArray, Value: Value) { MyArray in
                
                self.QuestionFourteenMultiArray = MyArray
                print("Fourteen Array :-" , MyArray)
                
            }
            
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
          ApiToken.SetArray(Key: "QuestionFourteenAnswer", Value: [])
        print("Ok")
        
        ApiToken.SetItems(Key: "QuestionFourteenComment", Value: "")
        
    }
    
    
    
    @IBAction func NextActionBu(_ sender: UIButton) {
        ApiToken.SetArrayOfDictionary(Key: "QuestionFourteenAnswer", Value: QuestionFourteenMultiArray)
        ApiToken.SetItems(Key: "QuestionFourteenComment", Value: self.QuestionComment.text)
    }
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArrayOfDictionary(Key: "QuestionFourteenAnswer", Value: QuestionFourteenMultiArray)
        ApiToken.SetItems(Key: "QuestionFourteenComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
            }
            
        }
    }
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer14 = Value
        self.NextButton.isHidden = false
        
    }
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
}




