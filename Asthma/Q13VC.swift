//
//  Q13VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q13VC: UIViewController {

    var QuestionAnswer13 : String!
    
    var QuestionThirteenArray = [String]()
    
    @IBOutlet weak var ButtonOne: UIButton! // 55
    
    @IBOutlet weak var buttonTwo: UIButton!  //56
    
    @IBOutlet weak var ButtonThree: UIButton!  //57
    
    @IBOutlet weak var ButtonFour: UIButton!  //58
    
    @IBOutlet weak var ButtonFive: UIButton!  // 59
    
    @IBOutlet weak var ButtonSex: UIButton!  // 60
    
    @IBOutlet weak var ButtonSeven: UIButton! // 61
    
    @IBOutlet weak var ButtonEight: UIButton!  //62
    
    @IBOutlet weak var ButtonNine: UIButton! //218
    
    
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true

        ApiMethods.GetQuestionAnswer(Question_id: "12"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionThirteenArray.append(obj!)

                    let FirstValue = "55"
                    let SeconValue = "56"
                    let ThirdValue = "57"
                    let FourthValue = "58"
                    let FifthValue = "59"
                    let SexValue = "60"
                    let SevnthValue = "61"
                    let Eightvalue = "62"
                    let NineValue = "218"
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.buttonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                    // Button Nine
                    if  obj == NineValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonNine, Value: NineValue)
                    }
                }
            }
            print("MyTest Array :-" , self.QuestionThirteenArray)
            
        }
        
        
    }
    
    @IBAction func NumberOne(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "55")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirteenArray, Value: "55") { MyArray in
            
            self.QuestionThirteenArray = MyArray
            
        }
    }
    
    
    @IBAction func NumberTwo(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "56")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirteenArray, Value: "56") { MyArray in
            
            self.QuestionThirteenArray = MyArray
            
        }
    }
    
    
    @IBAction func NumberThree(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "57")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirteenArray, Value: "57") { MyArray in
            
            self.QuestionThirteenArray = MyArray
            
        }

    }
    
    
    @IBAction func NumberFour(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "59")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirteenArray, Value: "59") { MyArray in
            
            self.QuestionThirteenArray = MyArray
            
        }
    }
    
    
    @IBAction func NumberFive(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "60")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirteenArray, Value: "60") { MyArray in
            
            self.QuestionThirteenArray = MyArray
            
        }

    }
    
    @IBAction func NumberSex(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "61")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirteenArray, Value: "61") { MyArray in
            
            self.QuestionThirteenArray = MyArray
            
        }

    }
    
    
    @IBAction func NumberSeven(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "58")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirteenArray, Value: "58") { MyArray in
            
            self.QuestionThirteenArray = MyArray
            
        }
    }
    
    @IBAction func NumberEight(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "62")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirteenArray, Value: "62") { MyArray in
            
            self.QuestionThirteenArray = MyArray
            
        }
    }
    
    @IBAction func NumberNineActionBu(_ sender: UIButton) {
      //  QuestionAnswer(Button: sender, Value: "218")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirteenArray, Value: "218") { MyArray in
            
            self.QuestionThirteenArray = MyArray
            
        }
    }
    @IBAction func SkipActionBu(_ sender: UIButton) {
        print("Ok")
        
        ApiToken.SetArray(Key: "QuestionThirteenAnswer", Value: [])
        ApiToken.SetItems(Key: "QuestionThirteenComment", Value: "")

    }
    
    
    @IBAction func NextButtonAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirteenAnswer", Value: QuestionThirteenArray)
        ApiToken.SetItems(Key: "QuestionThirteenComment", Value: self.QuestionComment.text)
    }
    
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirteenAnswer", Value: QuestionThirteenArray)
        ApiToken.SetItems(Key: "QuestionThirteenComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer13 = Value
        self.NextButton.isHidden = false

    }
    
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
}




