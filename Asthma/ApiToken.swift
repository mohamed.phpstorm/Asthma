//
//  ApiToken.swift
//  Asthma
//
//  Created by MOHAMED on 5/21/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import SwiftyJSON
class ApiToken {
    class func restartApp(){
        guard let window =  UIApplication.shared.keyWindow else{return}
        let sb = UIStoryboard(name: "Main", bundle: nil)
        var vc :UIViewController
        if getApiToken() == nil {
            // Skip Auth Screen
            vc = sb.instantiateInitialViewController()!
        }else{
            vc = sb.instantiateViewController(withIdentifier: "HomeViewController")
        }
        window.rootViewController = vc
        UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromTop, animations: nil, completion: nil)
    }
    
    
    class func setApiToken(Token : String)  {
        let api_token = UserDefaults.standard
        api_token.set(Token, forKey: "api_token")
        api_token.synchronize()
        restartApp()
    }
    
    class func getApiToken() -> String? {
        let api_token = UserDefaults.standard
        let obj =  api_token.object(forKey: "api_token")
        return obj as? String
    }
    
    // Add  String Value To User Default

    class func SetItems(Key : String! , Value : String!) {
        
        let Item = UserDefaults.standard
        Item.set(Value, forKey: Key )
        
    }
    // Get String Value From User Default
    class func GetItem(Key : String!) -> String!{
        let item = UserDefaults.standard
        if let obj = item.object(forKey: Key) {
            return obj as! String
        } else {
            return ""
        }
        
    }
    
    
    // Add Array Value To User Default
    class func SetArray(Key : String! , Value : [String]) {
        
        let Item = UserDefaults.standard
        Item.set(Value, forKey: Key )
    }
 
    // Add Array Dictionary
    class func SetArrayOfDictionary(Key : String! , Value : [Dictionary <String , [String]>]) {
        
        let Item = UserDefaults.standard
        Item.set(Value, forKey: Key )
    }
    
    // Change CheckBoxes Image
     class func ChangeCheckBoxImage(CheckBox:UIButton) {
        
        if CheckBox.currentImage == #imageLiteral(resourceName: "Checked") {
            CheckBox.setImage(#imageLiteral(resourceName: "UnChecked"), for: .normal)
            //isChecked = true
            
        }else if CheckBox.currentImage == #imageLiteral(resourceName: "UnChecked")  {
            CheckBox.setImage(#imageLiteral(resourceName: "Checked"), for: .normal)
            //isChecked = false

        }
     
    }
    class func SetDictionary (Key : String! , Value : Dictionary <String , String>) {
    let Item = UserDefaults.standard
    Item.set(Value, forKey: Key )
    }
    // End Of CheckBox Image
    // Set Array Value 
    
    class func SetArrayValue(Sender : UIButton , Questiontest : [String],Value : String , compltion : @escaping (_ QuestionArray : [String])->Void) {
        
        var QuestionArray = Questiontest
        if Sender.currentImage == #imageLiteral(resourceName: "Checked") {
            QuestionArray.append(Value)
        } else if Sender.currentImage == #imageLiteral(resourceName: "UnChecked") {
            if QuestionArray.contains(Value) {
                if let index = QuestionArray.index(of: Value) {
                    QuestionArray.remove(at: index)
                }
            }
        }
        compltion(QuestionArray)

    }
    // Sett Array For Question 14
    class func SetArrayQuestionFourteen(Sender : UIButton ,TextValue : String, QuestionArray : [Dictionary <String , [String]>],Value : String , compltion : @escaping (_ QuestionArray : [Dictionary <String , [String]>])->Void) {
        
        var QuestionArray = QuestionArray
        //var FirstArray = [JSO]()
        var SecondArray = [String]()
        if Sender.currentImage == #imageLiteral(resourceName: "Checked") {
           
            if TextValue == "199" {
                SecondArray.append("199")
            } else if TextValue == "200" {
                SecondArray.append("200")
            } else if TextValue == "201" {
                SecondArray.append("201")
            } else if TextValue == "202" {
                SecondArray.append("202")
            }
           var AnswersDict = Dictionary <String , [String]>()
            AnswersDict.updateValue(SecondArray, forKey: Value)
            QuestionArray.append(AnswersDict)
            print("My Dict Array :" ,QuestionArray)
        } else if Sender.currentImage == #imageLiteral(resourceName: "UnChecked") {
            let result = QuestionArray.filter{ $0[Value] == nil }
            QuestionArray = result
            print("My Dict Array After Remove :" ,QuestionArray)

        }
        compltion(QuestionArray)
        
    }
    class func SetDictionaryArray(Sender : UIButton ,TextValue : String,  Value : String,compilation : @escaping (_ QuestionArray : Dictionary <String , String> , _ value : String , _ text : String)->Void) {
        print("Button Value" , Value)
        print("Text Value" , TextValue)
        var SecondValue = String()
        if Sender.currentImage == #imageLiteral(resourceName: "Checked") {
            SecondValue = TextValue
            var AnswersDict = Dictionary <String , String>()
            AnswersDict[Value] = TextValue
           // AnswersDict.updateValue(SecondValue, forKey: Value)
            compilation(AnswersDict , Value , TextValue)
        }
    }
    // Get Question 14 Array 
    class func GetQuestionFourteenArray(Key : String!) -> [Dictionary <String , [String]>]{
    let item = UserDefaults.standard
    if let obj = item.object(forKey: Key) {
    return obj as! [Dictionary <String , [String]>]
    }else {
    return []
    }

    }
    // Get Dictionary
    class func GetDict(key : String )->Dictionary <String , String> {
        let item = UserDefaults.standard
        if let obj = item.object(forKey: key) {
            return obj as!  Dictionary <String , String>
        }else {
            return [:]
        }
    }
    // Get Array Value From User Default
    class func GetArray (Key : String!) -> [String] {
        let item = UserDefaults.standard
        if let obj = item.object(forKey: Key) {
            return obj as! [String]
        }else {
            return []
        }
    }
    // check if value exists in Question Answer
    class func CheckAnswerValue (Sender : UIButton ,QuestionArray : [String] , Value : String) {
        if QuestionArray.contains(Value) {
            Sender.setImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        }else {
            Sender.setImage(#imageLiteral(resourceName: "UnChecked"), for: .normal)
        }
    }
    
    // Check Question Value From Server
    
    class func CheckQuestionValue(Button : UIButton , Value : String ){
        Button.setImage(#imageLiteral(resourceName: "Checked"), for: .normal)
        
    }
 
    
    class func AlertAction(message : String, title : String , VC : UIViewController){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler:nil))
        VC.present(alert, animated: true, completion: nil)
        
    }
    
    
}
