//
//  HCpCell.swift
//  Asthma
//
//  Created by MOHAMED on 6/6/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit

class HCpCell: UITableViewCell {

    @IBOutlet weak var HcpId: UILabel!
    @IBOutlet weak var HcpName: UILabel!
    func ConfigureCell (User : HCPModel) {
        self.HcpName.text = User.name
        self.HcpId.isHidden = true

    }
    
   
}
