//
//  Q1VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/14/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q1VC: UIViewController {
    var QuestionOneArray  = [String]()

    var Hcp_Id : String!
    
    
    @IBOutlet weak var ButtonOne: UIButton!
    
    @IBOutlet weak var ButtonTwo: UIButton!
    
    @IBOutlet weak var ButtonThree: UIButton!
    
    @IBOutlet weak var ButtonFour: UIButton!
  
    @IBOutlet weak var MidAsthma: UITextField!
    
    @IBOutlet weak var ModerateAsthma: UITextField!
    var QuestionAnswer1 : String!
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        self.ModerateAsthma.text = ""
        self.MidAsthma.text = ""
        super.viewDidLoad()
        self.NextButton.isHidden = true
        if  self.Hcp_Id == nil {
            if let SecondId  = ApiToken.GetItem(Key: "Hcp_Id") {
                self.Hcp_Id = SecondId
                print( "Second",SecondId)
            }
        } else {
            
            UserDefaults.standard.removeObject(forKey: "Hcp_Id")
            RemoveData.removeDataObj()
            ApiToken.SetItems(Key: "Hcp_Id", Value: self.Hcp_Id)
            print("First " , ApiToken.GetItem(Key: "Hcp_Id"))

        }
        if let api_token = ApiToken.getApiToken() {
            if let HcpId = ApiToken.GetItem(Key: "Hcp_Id") {
                ApiMethods.GetModerate(api_token: api_token, HcpId: HcpId, questionId: "1"){ (error, status, mild, mod) in
                    if error == nil {
                        if mild != nil {
                            self.MidAsthma.text = mild
                        }
                        if mod != nil {
                            self.ModerateAsthma.text = mod
                        }
                    }
                }
        }
        }
       ApiMethods.GetQuestionAnswer(Question_id: "1"){(answers , error, comment) in
            if answers != nil {
                self.NextButton.isHidden = false
                print ("Answers For Comment" , comment)
                 self.QuestionComment.text = comment
                for obj in answers! {
                   
                    var obj = obj["answer_id"].string
                    self.QuestionOneArray.append(obj!)
                    let FirstValue = "1"
                    let SeconValue = "2"
                    let ThirdValue = "3"
                    let FourthValue = "4"
                    let FifthValue = "5"
                    let SexValue = "6"
                    let SevnthValue = "7"
                    let Eightvalue = "8"
                    
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    /*
                    // Button Five 
                    if  obj == FifthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                    }
                    // Button Sex
                    if  obj == SexValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                    }
                    // Button Seven
                    if  obj == SevnthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                    }
                    // Button Eight
                    if  obj == Eightvalue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                    }
 */
                }
            }
                print("MyTest Array :-" , self.QuestionOneArray)
            
        }
  
        
    }

    @IBAction func NumberOne(_ sender: UIButton) {
  
        
        QuestionAnswer(Button: sender, Value: "1")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        // Add value To Array
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionOneArray, Value: "1") { MyArray in
            self.QuestionOneArray = MyArray

            
        }
    }
  
    
    
    @IBAction func From5To10(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "2")
        
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        // Add value To Array

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionOneArray, Value: "2") { MyArray in
            
            self.QuestionOneArray = MyArray
            
        }
    }
    
    
    @IBAction func From10To15(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "3")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        // Add value To Array
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionOneArray, Value: "3") { MyArray in
            
            self.QuestionOneArray = MyArray
            
        }

    }
    
    
    @IBAction func MoreThan15(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "4")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        // Add value To Array
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionOneArray, Value: "4") { MyArray in
            
            self.QuestionOneArray = MyArray
            
        }

    }
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionOneAnswer", Value: [])
        
        ApiToken.SetItems(Key: "QuestionOneComment", Value: "")
        
    }
    
    @IBAction func NextActionBu(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionOneAnswer", Value: self.QuestionOneArray)
        ApiToken.SetItems(Key: "QuestionMidAsthma", Value: self.MidAsthma.text)
        ApiToken.SetItems(Key: "QuestionOneComment", Value: self.QuestionComment.text)

        ApiToken.SetItems(Key: "QuestionModerateAsthma", Value: self.ModerateAsthma.text)
        print(self.QuestionAnswer1)
    }
    
    
    func QuestionAnswer(Button : UIButton , Value : String)  {
        self.QuestionAnswer1 = Value
        self.NextButton.isHidden = false
        
    }
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionOneAnswer", Value: self.QuestionOneArray)
        ApiToken.SetItems(Key: "QuestionOneComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
            }
            
        }
    }
    
    @IBAction func MenuBuAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuVC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    // Menu Button
    // Presentation
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
}
