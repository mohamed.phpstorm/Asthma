//
//  Q15VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit

import DLRadioButton

import SwiftyJSON
class Q15VC: UIViewController {

    var QuestionAnswer15 : String!
    var QuestionFifteenArray = [String]()
    // Button Atrr
    
    @IBOutlet weak var ButtonOne: UIButton! //71
    
    @IBOutlet weak var ButtonTwo: UIButton! //72
    
    @IBOutlet weak var ButtonThree: UIButton!// 73
    
    
    @IBOutlet weak var ButtonFour: UIButton! //74
    
    @IBOutlet weak var ButtonFive: UIButton! //219
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
        ApiMethods.GetQuestionAnswer(Question_id: "16"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionFifteenArray.append(obj!)

                    let FirstValue = "71"
                    let SeconValue = "72"
                    let ThirdValue = "73"
                    let FourthValue = "74"
                  let FifthValue = "219"
                    
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                    /*
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionFifteenArray)
            
        }
        
        
    }
    
    @IBAction func NumberOne(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "71")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFifteenArray, Value: "71") { MyArray in
            
            self.QuestionFifteenArray = MyArray
            
        }
    }
    
    
    @IBAction func NumberTwo(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "72")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFifteenArray, Value: "72") { MyArray in
            
            self.QuestionFifteenArray = MyArray
            
        }
    }
    
    @IBAction func NumberThree(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "73")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFifteenArray, Value: "73") { MyArray in
            
            self.QuestionFifteenArray = MyArray
            
        }

    }
    
    
    @IBAction func NumberFour(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "74")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFifteenArray, Value: "74") { MyArray in
            
            self.QuestionFifteenArray = MyArray
            
        }
    }
    
    @IBAction func NumberFive(_ sender: UIButton) {
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionFifteenArray, Value: "219") { MyArray in
            self.QuestionFifteenArray = MyArray
            
        }
    }
    @IBAction func SkipActionBu(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionFifteenAnswer", Value: [])
        
        ApiToken.SetItems(Key: "QuestionFifteenComment", Value: "")
        
    }
    
    
    @IBAction func NextButtonAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionFifteenAnswer", Value: QuestionFifteenArray)
        ApiToken.SetItems(Key: "QuestionFifteenComment", Value: self.QuestionComment.text)
    }
    
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionFifteenAnswer", Value: QuestionFifteenArray)
        ApiToken.SetItems(Key: "QuestionFifteenComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer15 = Value
        self.NextButton.isHidden = false
        
        
    }
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
}




