//
//  Q20UpdatedVC.swift
//  Asthma
//
//  Created by MOHAMED on 11/21/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit

class Q20UpdatedVC: UIViewController {
    var QuestionAnswer8 : String!
    
    var QuestionEightArray = [String]()
    
    // Button Atrr
    @IBOutlet weak var ButtonOne: UIButton!  // 36
    @IBOutlet weak var ButtonTwo: UIButton! // 37
    @IBOutlet weak var ButtonThree: UIButton!  // 38
    @IBOutlet weak var ButtonFour: UIButton! // 39
    
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
        
        ApiMethods.GetQuestionAnswer(Question_id: "19"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment
                
                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionEightArray.append(obj!)
                    
                    let FirstValue = "228"
                    let SeconValue = "229"
                    let ThirdValue = "230"
                    let FourthValue = "231"
                    
                    
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    /*
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionEightArray)
            
        }
        
        
    }
    @IBAction func TheOne(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "228")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEightArray, Value: "228") { MyArray in
            
            self.QuestionEightArray = MyArray
            
        }
    }
    
    @IBAction func TheTwo(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "229")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEightArray, Value: "229") { MyArray in
            
            self.QuestionEightArray = MyArray
            
        }
    }
    
    @IBAction func TheThree(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "230")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEightArray, Value: "230") { MyArray in
            
            self.QuestionEightArray = MyArray
            
        }
    }
    
    
    @IBAction func TheFour(_ sender: UIButton) {
        QuestionAnswer(Button: sender, Value: "231")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionEightArray, Value: "231") { MyArray in
            
            self.QuestionEightArray = MyArray
            
        }
        
    }
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        
        ApiToken.SetArray(Key: "QuestionTwentyUpdatedAnswer", Value: [])
        ApiToken.SetItems(Key: "QuestionTwentyUpdatedComment", Value: "")
        
        
    }
    
    @IBAction func NextButtonAction(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionTwentyUpdatedAnswer", Value: QuestionEightArray)
        ApiToken.SetItems(Key: "QuestionTwentyUpdatedComment", Value: self.QuestionComment.text)
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionTwentyUpdatedAnswer", Value: QuestionEightArray)
        ApiToken.SetItems(Key: "QuestionTwentyUpdatedComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
            }
            
        }
    }
    
    func QuestionAnswer(Button : UIButton , Value : String)  {
        self.QuestionAnswer8 = Value
        self.NextButton.isHidden = false
        
    }
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
}





