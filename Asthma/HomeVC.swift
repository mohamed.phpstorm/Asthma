//
//  HomeVC.swift
//  Asthma
//
//  Created by MOHAMED on 6/6/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import Kingfisher
class HomeVC: UIViewController {

    @IBOutlet weak var UserNameText: UILabel!
    
    @IBOutlet weak var lastLoginText: UILabel!
  
    @IBOutlet weak var UserImage: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.GetMedicalRepData(api_token: api_token){ (error , last_login , name , photo) in
                if error == nil {
                    self.lastLoginText.text = last_login
                    self.UserNameText.text = name
                    if photo != nil {
                        let ImageUrl = URL(string: ImageRoot + photo!)!
                        self.UserImage.kf.setImage(with: ImageUrl, for: .normal)
                    }else {
                        self.UserImage.setImage(#imageLiteral(resourceName: "Avatar"), for: .normal)
                    }
                }
            }
            if let oneSignalToken = ApiToken.GetItem(Key: "OneSignalToken") {
                ApiMethods.AddNotify(api_token: api_token, token: oneSignalToken)
            }

        }
        
    }

 
    @IBAction func NotificationAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SchedualScreen") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }

    @IBAction func LogoutAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutScreen") as! LogoutVC
        self.present(vc, animated: true, completion: nil)
    }

    @IBAction func MenuBuAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuVC
        self.present(vc, animated: true, completion: nil)
        
    }
}
