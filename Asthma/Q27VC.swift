//
//  Q27VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
class Q27VC: UIViewController {

    var QuestionAnswer27 : String!
    var QuestionTwentySevenArray = [String]()
    
    @IBOutlet weak var ButtonOne: UIButton!  //126
    
    @IBOutlet weak var ButtonTwo: UIButton!  //127
    
    @IBOutlet weak var ButtonThree: UIButton!  //128
    
    @IBOutlet weak var ButtonFour: UIButton!  //129
    
    @IBOutlet weak var ButtonFive: UIButton!  //130
    
    @IBOutlet weak var ButtonSex: UIButton!  //131
    
    @IBOutlet weak var ButtonSeven: UIButton!  //132
    
    @IBOutlet weak var ButtonEight: UIButton!  //133
    
    @IBOutlet weak var ButtonNine: UIButton! //222
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
        ApiMethods.GetQuestionAnswer(Question_id: "30"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionTwentySevenArray.append(obj!)

                    let FirstValue = "126"
                    let SeconValue = "127"
                    let ThirdValue = "128"
                    let FourthValue = "129"
                    let FifthValue = "130"
                    let SexValue = "131"
                    let SevnthValue = "132"
                    let Eightvalue = "133"
                    let NineValue = "222"
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                    if  obj == NineValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonNine, Value: NineValue)
                    }
                }
            }
            print("MyTest Array :-" , self.QuestionTwentySevenArray)
            
        }
        
        
    }
    
    
    @IBAction func NumberOne(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "126")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentySevenArray, Value: "126") { MyArray in
            
            self.QuestionTwentySevenArray = MyArray
            
        }

    }
    
    
    
    @IBAction func NumberTwo(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "127")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentySevenArray, Value: "127") { MyArray in
            
            self.QuestionTwentySevenArray = MyArray
            
        }

    }
    
    @IBAction func NumberThree(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "128")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentySevenArray, Value: "128") { MyArray in
            
            self.QuestionTwentySevenArray = MyArray
            
        }

    }
    
    @IBAction func NumberFour(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "129")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentySevenArray, Value: "129") { MyArray in
            
            self.QuestionTwentySevenArray = MyArray
            
        }

    }
    
    @IBAction func NumberFive(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "130")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentySevenArray, Value: "130") { MyArray in
            
            self.QuestionTwentySevenArray = MyArray
            
        }

    }
    
    @IBAction func NumberSex(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "131")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentySevenArray, Value: "131") { MyArray in
            
            self.QuestionTwentySevenArray = MyArray
            
        }

    }
    
    @IBAction func NumberSeven(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "132")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentySevenArray, Value: "132") { MyArray in
            
            self.QuestionTwentySevenArray = MyArray
            
        }

    }
    
    @IBAction func NumberEight(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "133")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentySevenArray, Value: "133") { MyArray in
            
            self.QuestionTwentySevenArray = MyArray
            
        }

    }
    @IBAction func NumberNine(_ sender: UIButton) {
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentySevenArray, Value: "222") { MyArray in
            
            self.QuestionTwentySevenArray = MyArray
            
        }
    }
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionTwentySevenAnswer", Value: [])
        
        ApiToken.SetItems(Key: "QuestionTwentySevenComment", Value: "")
        
    }
    
    
    @IBAction func NextActionButton(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionTwentySevenAnswer", Value: self.QuestionTwentySevenArray)
        ApiToken.SetItems(Key: "QuestionTwentySevenComment", Value: self.QuestionComment.text)
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionTwentySevenAnswer", Value: self.QuestionTwentySevenArray)
        ApiToken.SetItems(Key: "QuestionTwentySevenComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer27 = Value
        self.NextButton.isHidden = false
  
    }
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
}




