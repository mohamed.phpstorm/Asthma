//
//  SeverCell.swift
//  Asthma
//
//  Created by MOHAMED on 6/27/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit

class SeverCell: UITableViewCell {

    @IBOutlet weak var ArticleImage: UIImageView!
    @IBOutlet weak var ArticleDate: UILabel!
    @IBOutlet weak var ArticleDescription: UITextView!
    @IBOutlet weak var ArticleTitle: UILabel!
    @IBOutlet weak var ArticleMainImage: UIImageView!
    
    @IBOutlet weak var FileAttach: UILabel!
    func ConfigureCell(Article : SeverAsthmaModel){
        self.ArticleDate.text = Article.created_date
        self.ArticleDescription.text = Article.details
        self.ArticleTitle.text = Article.title
        self.FileAttach.text = Article.attachment
        self.FileAttach.isHidden = true
    }

}
