//
//  Q35VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q35VC: UIViewController {

    var QuestionAnswer35 : String!
    
    var QuestionThirtFiveArray = [String]()
    
    
    @IBOutlet weak var ButtonOne: DLRadioButton!  // 173
    
    
    @IBOutlet weak var ButtonTwo: DLRadioButton!   //172
    
    
    @IBOutlet weak var ButtonThree: DLRadioButton!  //174
    
    
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
        ApiMethods.GetQuestionAnswer(Question_id: "38"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    let FirstValue = "173"
                    let SeconValue = "172"
                    let ThirdValue = "174"
                    var obj = obj["answer_id"].string

                    self.QuestionThirtFiveArray.append(obj!)

                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }/*
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionThirtFiveArray)
            
        }
        
        
    }
    
    
    @IBAction func NumberOne(_ sender: DLRadioButton) {
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        QuestionAnswer(Button: sender, Value: "173")
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtFiveArray, Value: "173") { MyArray in
            
            self.QuestionThirtFiveArray = MyArray
            
        }
    }
    
    
    @IBAction func NumberTwo(_ sender: DLRadioButton) {
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        QuestionAnswer(Button: sender, Value: "172")
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtFiveArray, Value: "172") { MyArray in
            
            self.QuestionThirtFiveArray = MyArray
            
        }
    }
    
    @IBAction func NumberThree(_ sender: DLRadioButton) {
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        QuestionAnswer(Button: sender, Value: "175")
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtFiveArray, Value: "175") { MyArray in
            
            self.QuestionThirtFiveArray = MyArray
            
        }
    }
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionThirtyFiveAnswer", Value: [])
        
        ApiToken.SetItems(Key: "QuestionThirtyFiveComment", Value: "")
    }
    
    
    
    @IBAction func NextButtonAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirtyFiveAnswer", Value: QuestionThirtFiveArray)
        ApiToken.SetItems(Key: "QuestionThirtyFiveComment", Value: self.QuestionComment.text)
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirtyFiveAnswer", Value: QuestionThirtFiveArray)
        ApiToken.SetItems(Key: "QuestionThirtyFiveComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer35 = Value
        self.NextButton.isHidden = false
        
        
    }
    
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
}




