//
//  Q12VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q12VC: UIViewController {
    var QuestionAnswer12 : String!
    
    var QuestionTwelveArray = [String]()
    var TextArray = [String]()
    @IBOutlet weak var FirstPercentage: UITextField!
    
    @IBOutlet weak var SecondPercentage: UITextField!
    
    @IBOutlet weak var ThirdPercentage: UITextField!
    
    @IBOutlet weak var FourthPercentage: UITextField!
    
    @IBOutlet weak var FifthPercentage: UITextField!
    
    
    @IBOutlet weak var SexPercentage: UITextField!
    
    
    var QuestionFourteenMultiArray = Dictionary <String , String>()

    
    
    
    
    @IBOutlet weak var ButtonOne: UIButton! // 51
    
    @IBOutlet weak var ButtonTwo: UIButton! // 52
    
    
    @IBOutlet weak var ButtonThree: UIButton! // 53
    
    @IBOutlet weak var ButtonFour: UIButton! // 54
    
    @IBOutlet weak var ButtonFive: UIButton! //210
    
    @IBOutlet weak var ButtonSex: UIButton!//211
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true

        ApiMethods.GetQuestionAnswer(Question_id: "11"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for answer in answers! {
                    var obj = answer["answer_id"].string
                    var Percent = answer["percentage"].string
                    self.QuestionTwelveArray.append(obj!)

                    let FirstValue = "51"
                    let SeconValue = "52"
                    let ThirdValue = "53"
                    let FourthValue = "54"
                    let FifthValue = "210"
                    let SexValue = "211"
                    
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                        if Percent != nil {
                            self.FirstPercentage.text = Percent
                        }
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                        if Percent != nil {
                            self.SecondPercentage.text = Percent
                        }
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                        if Percent != nil {
                            self.ThirdPercentage.text = Percent
                        }
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                        if Percent != nil {
                            self.FourthPercentage.text = Percent
                        }
                    }
                    
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                        if Percent != nil {
                            self.FifthPercentage.text = Percent
                        }
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                        if Percent != nil {
                            self.SexPercentage.text = Percent
                        }
                     }
                    /*
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionTwelveArray)
            
        }
        
        
    }
    @IBAction func NumberOne(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "51")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwelveArray, Value: "51") { MyArray in
            
            self.QuestionTwelveArray = MyArray
            
        }
    }
    
    @objc func ChecktextField(Text : UITextField , Sender : UIButton , Value : String) {
        print("Checked")
        if(Text.text?.isEmpty)! {
            self.SetItems(value: "", button: Sender , Value : Value)
            
        }else {
            self.SetItems(value: Text.text!, button: Sender , Value : Value)
            return
        }
    }
    func SetItems(value : String , button : UIButton , Value : String) {
        
        ApiToken.SetDictionaryArray(Sender: button, TextValue: value, Value: Value) { (MyArray , key , keyval ) in
            self.QuestionFourteenMultiArray[key] = keyval
            print("Dictionary Array" , MyArray)
        }
    }
        
    @IBAction func NumberTwo(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "52")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwelveArray, Value: "52") { MyArray in
            
            self.QuestionTwelveArray = MyArray
            
        }
    }
    
    @IBAction func NumberThree(_ sender: DLRadioButton) {
        
        QuestionAnswer(Button: sender, Value: "53")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwelveArray, Value: "53") { MyArray in
            
            self.QuestionTwelveArray = MyArray
            
        }
    }
    
    
    @IBAction func NumberFour(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "54")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwelveArray, Value: "54") { MyArray in
            
            self.QuestionTwelveArray = MyArray
            
        }
    }
    
    @IBAction func NumberFive(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "210")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwelveArray, Value: "210") { MyArray in
            
            self.QuestionTwelveArray = MyArray
            
        }
    }
    
    
    @IBAction func NumberSex(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "211")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwelveArray, Value: "211") { MyArray in
            
            self.QuestionTwelveArray = MyArray
            
        }
    }
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        print("Ok")
        ApiToken.SetDictionary(Key: "QuestionElevenPercentage", Value: [:])

        ApiToken.SetArray(Key: "QuestionTwelveAnswer", Value: [])
        ApiToken.SetItems(Key: "QuestionTwelveComment", Value: "")

    }

    @IBAction func NextButtonAction(_ sender: UIButton) {
        self.ChecktextField(Text: self.FirstPercentage, Sender: ButtonOne, Value: "51")
        self.ChecktextField(Text: self.SecondPercentage, Sender: ButtonTwo, Value: "52")
        self.ChecktextField(Text: self.ThirdPercentage, Sender: ButtonThree, Value: "53")
        self.ChecktextField(Text: self.FourthPercentage, Sender: ButtonFour, Value: "54")
        self.ChecktextField(Text: self.FifthPercentage , Sender: ButtonFive, Value: "210")
        self.ChecktextField(Text: self.SexPercentage , Sender: ButtonSex, Value: "211")
        ApiToken.SetArray(Key: "QuestionTwelveAnswer", Value: QuestionTwelveArray)
        ApiToken.SetItems(Key: "QuestionTwelveComment", Value: self.QuestionComment.text)
        ApiToken.SetDictionary(Key: "QuestionElevenPercentage", Value: QuestionFourteenMultiArray)

        //QuestionElevenPercentage
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        self.ChecktextField(Text: self.FirstPercentage, Sender: ButtonOne, Value: "51")
        self.ChecktextField(Text: self.SecondPercentage, Sender: ButtonTwo, Value: "52")
        self.ChecktextField(Text: self.ThirdPercentage, Sender: ButtonThree, Value: "53")
        self.ChecktextField(Text: self.FourthPercentage, Sender: ButtonFour, Value: "54")
        self.ChecktextField(Text: self.FifthPercentage , Sender: ButtonFive, Value: "210")
        self.ChecktextField(Text: self.FifthPercentage , Sender: ButtonFive, Value: "211")
        ApiToken.SetDictionary(Key: "QuestionElevenPercentage", Value: QuestionFourteenMultiArray)

        ApiToken.SetArray(Key: "QuestionTwelveAnswer", Value: QuestionTwelveArray)
        ApiToken.SetItems(Key: "QuestionTwelveComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer12 = Value
        self.NextButton.isHidden = false
 
        
    }
    
    
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
}




