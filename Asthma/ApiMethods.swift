//
//  ApiMethods.swift
//  Asthma
//
//  Created by MOHAMED on 5/21/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ApiMethods {
    
    // Reset Password
    
    class func MedicalRepResetPassword(email: String , compltion : @escaping (_ status: Int? , _ message :String?, _ error: Error?)->Void) {
        let parametes = [
            "email" : email
        ]
        
        let url = URL(string: ResetPasswordUrl)!
        Alamofire.request(url, method: .post, parameters: parametes, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let myerror) :
                print(myerror)
                let error = JSON(myerror)
                
                let status = error["status"].int
                let message = error["msg"].string
                compltion(status  , message,myerror)
                return
            case .success(let value):
                let json = JSON(value)
                let status = json["status"].int
                let message = json["msg"].string
                let data = json["data"]
                
                
                compltion(status!, message!, nil)
                return
                
                
            }
        }
    }
    
    
    
    // Add Device For Notificaion
    class func AddNotify(api_token : String , token : String ) {
        let parameters = [
            
            "api_token" : api_token ,
            "token" : token ,
            "type" : "ios"
            
        ]
        let url = URL(string: RegisterNotifyUrl)!
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let myerror) :
                print(myerror)
                let error = JSON(myerror)
                print(error)
                
            case .success(let value):
                let json = JSON(value)
                print(json)
                
            }
        }
        
    }
    
    
    // Register
    
    class func MedicalRepRegister( name:String ,password: String , email: String,  password_confirmation:String , compltion : @escaping (_ status: Int? , _ message :String?, _ error: Error?)->Void) {
        let parametes = [
            "name" : name,
            "password" : password,
            "email" : email,
            "password_confirmation" : password_confirmation
        ]
        
        let url = URL(string: RegisterUrl)!
        Alamofire.request(url, method: .post, parameters: parametes, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let myerror) :
                print(myerror)
                let error = JSON(myerror)
                
                let status = error["status"].int
                let message = error["msg"].string
                compltion(status  , message,myerror)
                return
            case .success(let value):
                let json = JSON(value)
                let status = json["status"].int
                let message = json["msg"].string
                let data = json["data"]
                print(data)
                
                let user = data["user"].dictionary
                if let UserName = user?["name"]?.string {
                    ApiToken.SetItems(Key: "User_id", Value: UserName)
                }
                if let login_date = user?["created_at"]?.string {
                    print(login_date)
                    ApiToken.SetItems(Key: "last_login", Value: login_date)
                }
                if let UserImage = user?["photo"]?.string {
                    print("User Image :-" ,UserImage)
                    ApiToken.SetItems(Key: "UserImage", Value: UserImage)
                }
                if let api_token = data["api_token"].string{
                    ApiToken.setApiToken(Token: api_token)
                }
                compltion(status!, message!, nil)
                return
                
                
            }
        }
    }
    
    // Login
    
    class func MedicalRepLogin (email : String , password : String ,compltion : @escaping (_ status: Int?,_ message:String? , _ error: Error?)->Void) {
        
        let parameters = [
            "email" : email ,
            "password" : password
        ]
        let url = URL(string: LoginUrl)!
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            switch response.result {
            case .failure(let error) :
                let json = JSON(error)
                let status = json["status"].int
                let message = json["msg"].string
                compltion(status, message , error)
                return
            case .success(let value) :
                let json = JSON(value)
                print(json)
                let data = json["data"]
                let status = json["status"].int
                let message = json["msg"].string
                let seconddata = data["data"].dictionary
                
                let user = seconddata?["user"]?.dictionary
                if let UserName = user?["name"]?.string {
                    print(UserName)
                    ApiToken.SetItems(Key: "User_id", Value: UserName)
                }
                let last_login = user?["last_login"]?.dictionary
                if let login_date = last_login?["date"]?.string {
                    print(login_date)
                    ApiToken.SetItems(Key: "last_login", Value: login_date)
                }
                if let UserImage = user?["photo"]?.string {
                    print("User Image :-" ,UserImage)
                    ApiToken.SetItems(Key: "UserImage", Value: UserImage)
                }
                if let api_token = seconddata?["api_token"]?.string{
                    ApiToken.setApiToken(Token: api_token)
                    
                }
                compltion(status, message, nil)
                return
                
            }
        }
        
    }
    
    // List All Doctors
    
    class func GetHcpDoctor(api_token : String!,page : Int = 1 ,keyword : String = "",compltion : @escaping (_ error:Error?,_ HCPDoctors: [HCPModel]? , _ last_page : Int)->Void) {
        print("My Test" , api_token)
        let url = URL(string: HcpDoctorUrl + "?api_token=" + api_token)!
        let parameters = [
            "page" : page ,
            "keyword" : keyword
            ] as [String : Any]
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
                
                
            case .failure(let error) :
                print(error)
                compltion(error, nil, page)
                return
            case .success(let value) :
                let json = JSON(value)
                var HCPDoctors = [HCPModel]()
                if let data = json["data"].dictionary {
                    print(data)
                    if let DictorData = data["data"]?.array{
                        for obj in DictorData {
                            if let obj = obj.dictionary {
                                let Doctor = HCPModel(HCPDict: obj)
                                print(obj)
                                HCPDoctors.append(Doctor)
                            }
                        }
                    }
                    
                }
                print(HCPDoctors)
                
                let last_page = json["last_page"].int ?? page
                compltion(nil, HCPDoctors, last_page)
                return
            }
        }
        
    }
    
    
    // Add Doctor
    
    class func AddHcpDoctor(api_token : String! ,name : String!, specialty : String! ,specialist_consultant: String! ,city: String! ,country: String! , authority : String! , hospital : String! ,sector : String! ,compltion : @escaping (_ error:Error?,_ status : Int?)->Void){
        let parametes = [
            "api_token" : api_token ,
            "name" : name ,
            "specialty" : specialty ,
            "specialist_consultant" : specialist_consultant ,
            "city" : city ,
            "country" : country ,
            "authority" : authority ,
            "hospital" : hospital ,
            "sector" : sector
            ] as [String : Any]
        let url  = URL(string: AddDoctorUrl)!
        
        Alamofire.request(url, method: .post, parameters: parametes, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error , 0)
                return
            case .success(let value):
                let json = JSON(value)
                print(json)
                let data = json["data"]
                
                
                let status = json["status"].int
                compltion(nil , status)
                return
                
            }
        }
        
    }
    
    
    // Edit Hcp Doctor
    class func EditHcpDoctor(api_token : String!, id : String! ,name : String!, specialty : String! ,specialist_consultant: String! ,city: String! ,country: String! , authority : String! , hospital : String! ,sector : String! ,compltion : @escaping (_ error:Error?,_ status : Int?)->Void){
        let parametes = [
            "api_token" : api_token ,
            "name" : name ,
            "specialty" : specialty ,
            "specialist_consultant" : specialist_consultant ,
            "city" : city ,
            "country" : country ,
            "authority" : authority ,
            "hospital" : hospital ,
            "sector" : sector
            ] as [String : Any]
        let url  = URL(string: EditDoctorUrl + id)!
        
        Alamofire.request(url, method: .post, parameters: parametes, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error , 0)
                return
            case .success(let value):
                let json = JSON(value)
                
                let data = json["data"]
                let status = json["status"].int
                compltion(nil , status)
                return
                
            }
        }
        
    }
    
    
    // Delete Doctor
    class func DeleteHcpDoctor(api_token : String! , id : String! ,compltion : @escaping (_ error:Error?,_ status : Int?)->Void){
        
        let parameters = [
            
            "api_token" : api_token
            ] as [String : Any]
        let url = URL(string: DeleteDoctorUrl + id)!
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error , 0)
                return
            case .success(let value):
                let json = JSON(value)
                print(json)
                let data = json["data"]
                let status = json["status"].int
                compltion(nil , status)
                return
                
            }
        }
    }
    
    // Add Questionier
    
    
    class func AddQuestionier(api_token : String!,compltion : @escaping (_ error:Error?,_ status : Int?)->Void){
        

        
        if let Hcp_id = ApiToken.GetItem(Key: "Hcp_Id"){
            print("M Hcp Doctor " , Hcp_id)
            print("QusyionFourteen Function Array :-", ApiToken.GetQuestionFourteenArray(Key: "QuestionFourteenAnswer"))
   ApiMethods.AddModerate(api_token: api_token, HcpId: Hcp_id, questionId: "1" , mild : ApiToken.GetItem(Key: "QuestionMidAsthma"), mold : ApiToken.GetItem(Key: "QuestionMidAsthma"))
            var parameters : [String : Any] = [
                "api_token" : api_token,
                "doctor_id" : Hcp_id
            ]
            
            var QuestionFive = ApiToken.GetDict(key: "QuestionFivePercentage")
            let QuestionEleven = ApiToken.GetDict(key: "QuestionElevenPercentage")
            if !QuestionEleven.isEmpty {
            for (theKey, theValue) in QuestionEleven {
                QuestionFive[theKey] = theValue
                print("Questions", theKey)
            }
            }
            parameters["answers"] = [
                [
                    "question_id" : "1",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionOneAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionOneComment"),
                ],
                [
                    "question_id" : "2",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionTwoAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionTwoComment"),

                    
                ],
                [
                    "question_id" : "3",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionThreeAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionThreeComment"),

                ],
                [
                    "question_id" : "4",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionFourAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionFourComment"),

                ],
                [
                    "question_id" : "5",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionFiveAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionFiveComment"),


                ],
                [
                    "question_id" : "19",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionSexAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionSexComment"),

                ],
                [
                    "question_id" : "6",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionSevenAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionSevenComment"),

                ],
                [
                    "question_id" : "7",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionEightAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionEightComment"),

                ],
                [
                    "question_id" : "8",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionNineAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionNineComment"),

                ],
                [
                    "question_id" : "9",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionTenAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionTenComment"),

                ],
                [
                    "question_id" : "10",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionElevenAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionElevenComment"),

                ],
                [
                    
                    "question_id" : "11",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionTwelveAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionTwelveComment"),


                ],
                [
                    "question_id" : "12",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionThirteenAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionThirteenComment"),

                ],
                [
                    "question_id" : "13",
                    "answer_id" : ApiToken.GetQuestionFourteenArray(Key: "QuestionFourteenAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionFourteenComment"),

                ],
                [
                    "question_id" : "14",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionFifteenUpdatedAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionFifteenUpdatedComment"),

                ],
                [
                    "question_id" : "15",
                    "answer_id" : ApiToken.GetArray(Key: "QuiestionSixteenUpdatedAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuiestionSixteenUpdatedComment"),

                ],
                [
                    "question_id" : "16",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionFifteenAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionFifteenComment"),

                ],
                [
                    "question_id" : "17",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionSexteenAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionSexteenComment"),

                ],
                [
                    "question_id" : "18",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionSeventeenAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionSeventeenComment"),

                ],
                [
                    "question_id" : "21",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionEighteenAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionEighteenComment"),

                ],
                [
                    "question_id" : "22",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionNinteentAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionNinteenComment"),

                ],
                [
                    "question_id" : "23",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionTwentyAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionTwentyComment"),

                ],
                [
                    "question_id" : "24",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionTwentyOneAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionTwentyOneComment"),

                ],
                [
                    "question_id" : "25",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionTwentyTwoAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionTwentyTwoComment"),

                ],
                [
                    "question_id" : "27",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionTwentyThreeAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionTwentyThreeComment"),

                ],
                [
                    "question_id" : "28",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionTwentyFourAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionTwentyFourComment"),

                ],
                [
                    "question_id" : "26",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionTwentyFiveAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionTwentyFiveComment"),

                ],
                [
                    "question_id" : "29",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionTwentySexAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionTwentySexComment"),

                ],
                [
                    "question_id" : "30",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionTwentySevenAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionTwentySevenComment"),

                ],
                [
                    "question_id" : "31",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionTwentyEightAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionTwentyEightComment"),

                ],
                [
                    "question_id" : "32",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionTwentyNineAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionTwentyNineComment"),

                ],
                [
                    "question_id" : "33",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionThirtyAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionThirtyComment"),

                ],
                [
                    "question_id" : "34",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionThirtyOneAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionThirtyOneComment"),

                ],
                [
                    "question_id" : "35",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionThirtyTwoAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionThirtyTwoComment"),

                ],
                [
                    "question_id" : "36",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionThirtyThreeAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionThirtyThreeComment"),

                ],
                [
                    "question_id" : "37",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionThirtyFourAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionThirtyFourComment"),

                ],
                [
                    "question_id" : "38",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionThirtyFiveAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionThirtyFiveComment"),

                ],
                [
                    "question_id" : "39",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionThirtySexAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionThirtySexComment"),

                ],
                [
                    "question_id" : "40",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionThirtySevenAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionThirtySevenComment"),

                ],
                [
                    "question_id" : "41",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionThirtyEightAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionThirtyEightComment"),

                ],
                [
                    "question_id" : "42",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionThirtyNineAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionThirtyNineComment"),

                ],
                [
                    "question_id" : "43",
                    "answer_id" : ApiToken.GetArray(Key: "QuestionFourtyAnswer"),
                    "comment" : ApiToken.GetItem(Key: "QuestionFourtyComment"),

                ],
                [
                "question_id" : "20",
                "answer_id" : ApiToken.GetArray(Key: "QuestionTwentyUpdatedAnswer"),
                "comment" : ApiToken.GetItem(Key: "QuestionTwentyUpdatedComment"),

                ]
            ]
            print("Question Five" , QuestionFive)
            parameters["percentage"] = QuestionFive

              // parameters["percentage"] = ApiToken.GetDict(key: "QuestionElevenPercentage")

         //   print("Question5 Array " ,  ApiToken.GetArray(Key: "QuestionFivePercentage"))
            
            debugPrint(parameters.description)
            
            let url = URL.init(string:AddQuestionierUrl)
            
            var request = URLRequest(url: url!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            request.httpBody = try! JSONSerialization.data(withJSONObject: parameters)
            
            Alamofire.request(request).responseJSON { (response) in
                
                switch response.result {
                case .failure(let error):
                    print(error)
                    compltion(error, 0)
                    return
                case .success(let value):
                    let json = JSON(value)
                    compltion(nil, 1)
                    return
                }
                
            }
            
        }
    }
    
    class func AddScedual(api_token : String!,name : String! ,location: String!, start: String! , end : String! , repeataction : String! , alert : String! ,compltion : @escaping (_ error:Error?,_ status : Int?)->Void){
        
        print("Repeate Time :-"  , repeataction)
        print("Start Data :-" ,  start)
        print("Start Data :-" , end )
        print("Repeate Time :-"  , alert)

        let parameters = [
            "api_token" : api_token,
            "name" : name,
            "location" : location,
            "start" : start ,
            "end" : end ,
            "repeat" : repeataction,
            "alert" : alert
            ] as [String : Any]
        
        let url = URL(string: AddSchedualUrl)!
        print("Add Scheduale Url :" ,url)
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error , 0)
                return
            case .success(let value):
                let json = JSON(value)
                print(json)
                let data = json["data"]
                let status = json["status"].int
                compltion(nil , status)
                return
                
            }
        }
    }
    
    
    
    // Get All Articles
    
    
    class func GetAllArticels(api_token : String!,page : Int = 1 ,compltion : @escaping (_ error:Error?,_ Articles: [SeverAsthmaModel]? , _ last_page : Int)->Void) {
        
        let parameters = [
            
            "api_token" : api_token
            ] as [String : Any]
        
        
        let url = URL(string: GetArticelsUrl)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
                
                
            case .failure(let error) :
                print(error)
                compltion(error, nil, page)
                return
            case .success(let value) :
                let json = JSON(value)
                var Articles = [SeverAsthmaModel]()
                if let data = json["data"].dictionary {
                    print(data)
                    if let DictorData = data["data"]?.array{
                        for obj in DictorData {
                            if let obj = obj.dictionary {
                                let Article = SeverAsthmaModel(SeverDict: obj)
                                print(obj)
                                Articles.append(Article)
                            }
                        }
                    }
                    
                }
                
                let last_page = json["last_page"].int ?? page
                compltion(nil, Articles, last_page)
                return
            }
        }
        
    }
    
    
    
    // Patient Journey
    
    class func GetPatientJourney(api_token : String ,doctor_id : String! ,compltion : @escaping (_ error:Error?,_ FirstAnswer : [JSON]?,_ SecondAnswer : [JSON]? , _ ThirdAnswer : [JSON]? , _ FourthAnswer : [JSON]? , _ FifthAnswer : [JSON]? , _ SexAnswer : [JSON]? , _ SevenAnswer : [JSON]? , _ EightAnswer : [JSON]? , _ NineAnswer : [JSON]? , _ TenAnswer : [JSON]? ,_ ElevenAnswer : [JSON]? , _ TwelveAnswer : [JSON]? , _ ThirteenAnswer : [JSON]? , _ FourteenAnswer : [JSON]? , _ FifteenAnswer : [JSON]? , _ SexteenAnswer : [JSON]? , _ SeventeenAnswer : [JSON]? , _ EighteenAnswer : [JSON]? , _ NinteenAnswer : [JSON]? , _ TwentyAnswer : [JSON]? , _ TwentyOneAnswer : [JSON]? , _ TwentyTwoAnswer : [JSON]? , _ TwentyThreeAnswer : [JSON]? , _ TwentyFourAnswer : [JSON]?)->Void){
        
        let parameters = [
            
            "api_token" : api_token ,
            "doctor_id" : doctor_id
            ] as [String : Any]
        
        
        let url = URL(string: PatientJourneyUrl)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil)
                return
            case .success(let value):
                print(value)
                let json = JSON(value)
                let data = json["data"]
                let AnswerOne = data["answer_1"].array
                let AnswerTwo = data["answer_5"].array
                let AnswerThree = data["answer_6"].array
                let AnswerFour = data["answer_8"].array
                let AnswerFive = data["answer_9"].array
                let AnswerSex = data["answer_10"].array
                let AnswerSeven = data["answer_11"].array
                let AnswerEight = data["answer_12"].array
                let AnswerNine = data["answer_4"].array
                let AnswerTen = data["answer_13"].array
                let AnswerElevne = data["answer_14"].array
                let AnswerTwelve = data["answer_18"].array
                let AnswerThirteen = data["answer_19"].array
                let AnswerFourteen = data["answer_20"].array
                let AnswerFifteen = data["answer_21"].array
                let AnswerSexteen = data["answer_24"].array
                let AnswerSeventeen = data["answer_29"].array
                let AnswerEighteen = data["answer_31"].array
                let AnswerNinteen = data["answer_25"].array
                let AnswerTwenty = data["answer_26"].array
                let AnswerTwentyOne = data["answer_32"].array
                let AnswerTwentyTwo = data["answer_33"].array
                let AnswerTwentyThree = data["answer_38"].array
                let AnswerTwentyFour = data["answer_2"].array
                compltion(nil, AnswerOne, AnswerTwo, AnswerThree, AnswerFour, AnswerFive, AnswerSex, AnswerSeven, AnswerEight, AnswerNine, AnswerTen, AnswerElevne, AnswerTwelve, AnswerThirteen, AnswerFourteen, AnswerFifteen, AnswerSexteen, AnswerSeventeen, AnswerEighteen, AnswerNinteen, AnswerTwenty, AnswerTwentyOne, AnswerTwentyTwo, AnswerTwentyThree, AnswerTwentyFour)
                
            }
        }
    }
    
    // Get Quesyioner Doctor
    // get Questioner Doctors
    class func GetQuestionerHcpDoctor(api_token : String!,page : Int = 1 ,keyword : String = "",compltion : @escaping (_ error:Error?,_ HCPDoctors: [HCPModel]? , _ last_page : Int)->Void) {
        print("My Test" , api_token)
        let url = URL(string: HcpQuestionierUrl + "?api_token=" + api_token)!
        let parameters = [
            "page" : page ,
            "keyword" : keyword
            ] as [String : Any]
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
                
                
            case .failure(let error) :
                print(error)
                compltion(error, nil, page)
                return
            case .success(let value) :
                let json = JSON(value)
                var HCPDoctors = [HCPModel]()
                if let data = json["data"].dictionary {
                    print(data)
                    if let DictorData = data["data"]?.array{
                        for obj in DictorData {
                            if let obj = obj.dictionary {
                                let Doctor = HCPModel(HCPDict: obj)
                                print(obj)
                                HCPDoctors.append(Doctor)
                            }
                        }
                    }
                    
                }
                print(HCPDoctors)
                
                let last_page = json["last_page"].int ?? page
                compltion(nil, HCPDoctors, last_page)
                return
            }
        }
        
    }
    
    
    
    // list Of Reminder
    
    class func GetReminders(api_token : String?,compltion : @escaping (_ error:Error?,_ date : [JSON]?)->Void){
        
        
        let url = URL(string: ReminderListUrl + api_token!)!
        print(url)
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
                
            case .failure(let error) :
                print(error)
                compltion(error , nil)
                return
            case .success(let value) :
                let json = JSON(value)
                
                let data = json["data"].array
                print("Data :-",data)
                compltion(nil,data)
                return
                
            }
        }
    }
    
    // Get Report Hcp Doctor
    class func GetReportHcpDoctor(api_token : String!,page : Int = 1 ,keyword : String = "",compltion : @escaping (_ error:Error?,_ HCPDoctors: [HCPModel]? , _ last_page : Int)->Void) {
        print("My Test" , api_token)
        let url = URL(string: HcpPatientJourneyUrl + "?api_token=" + api_token)!
        let parameters = [
            "page" : page ,
            "keyword" : keyword
            ] as [String : Any]
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
                
                
            case .failure(let error) :
                print(error)
                compltion(error, nil, page)
                return
            case .success(let value) :
                let json = JSON(value)
                var HCPDoctors = [HCPModel]()
                if let data = json["data"].dictionary {
                    print(data)
                    if let DictorData = data["data"]?.array{
                        for obj in DictorData {
                            if let obj = obj.dictionary {
                                let Doctor = HCPModel(HCPDict: obj)
                                print(obj)
                                HCPDoctors.append(Doctor)
                            }
                        }
                    }
                    
                }
                print(HCPDoctors)
                
                let last_page = json["last_page"].int ?? page
                compltion(nil, HCPDoctors, last_page)
                return
            }
        }
        
    }
    
    
    
    // Get All Reminder List
    
    
    class func GetAllReminderList(api_token : String!,page : Int = 1 , date : String!,compltion : @escaping (_ error:Error?,_ Reminder: [ReminderList]? , _ last_page : Int)->Void) {
        print("My Test" , api_token)
        let url = URL(string: GetReminderByUserUrl)!
        let parameters = [
            "page" : page ,
            "start" : date,
            "api_token" : api_token
            ] as [String : Any]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
                
                
            case .failure(let error) :
                print(error)
                compltion(error, nil, page)
                return
            case .success(let value) :
                let json = JSON(value)
                var Reminders = [ReminderList]()
                if let ReminderData = json["data"].array{
                    for obj in ReminderData {
                        if let obj = obj.dictionary {
                            let reminder = ReminderList(ReminderDict : obj)
                            print(obj)
                            Reminders.append(reminder)
                        }
                    }
                }
                print(Reminders)
                
                let last_page = json["last_page"].int ?? page
                compltion(nil, Reminders, last_page)
                return
            }
        }
        
    }
    
    // Get Each Question Answer
    
    class func GetQuestionAnswer(Question_id : String,compltion : @escaping (_ answers : [JSON]? ,_ error : Error?, _ comment : String?)->Void){
        print("HCP ID :- " , ApiToken.GetItem(Key: "Hcp_Id"))
        if let api_token = ApiToken.getApiToken() {
            if let Hcp_id = ApiToken.GetItem(Key: "Hcp_Id"){
                let parameters = [
                    "api_token" : api_token,
                    "doctor_id" : Hcp_id,
                    "question_id" : Question_id
                    ] as [String : Any]
                let url = URL(string: SingleQuestionUrl)!
                Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                    switch response.result {
                    case .failure(let error) :
                        print(error)
                        compltion(nil, error ,  nil)
                        return
                    case .success(let value):
                        let json = JSON(value)
                        print("Test ID : Data" ,json)
                        var answers = [json]
                        var comment = ""
                        if let data = json["data"].dictionary {
                            if let QuestionAnswers = data["answers"]?.dictionary {
                                print("Comment Data" ,data )
                                if let Comment =  QuestionAnswers["comment"]?.string {
                                    comment = Comment
                                }
                                if let answer = QuestionAnswers["answer_id"]?.array {
                                    answers = answer
                                }
                            }
                        }
                        
                        compltion(answers, nil , comment)
                        return
                    }
                    
                }
                
            }
            
        }
    }
    
    // Get Question Fourteen Answers
    
    class func GetQuestionFourteenAnswers(Question_id : String,compltion : @escaping (_ answers : [JSON]? ,_ error : Error?)->Void){
        if let api_token = ApiToken.getApiToken() {
            if let Hcp_id = ApiToken.GetItem(Key: "Hcp_Id"){
                let parameters = [
                    "api_token" : api_token,
                    "doctor_id" : Hcp_id,
                    "question_id" : Question_id
                    ] as [String : Any]
                let url = URL(string: SingleQuestionUrl)!
                Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                    switch response.result {
                    case .failure(let error) :
                        print(error)
                        compltion(nil, error)
                        return
                    case .success(let value):
                        let json = JSON(value)
                        var answers = [json]
                        if let data = json["data"].dictionary {
                            print("Data :-", data)
                            if let QuestionAnswers = data["answers"]?.dictionary {
                                print("QuestionAnswers" ,QuestionAnswers)
                                if let answer = QuestionAnswers["answer_id"]?.dictionary {
                                    if let arrayValue = answer["val_array"]?.array {
                                        answers = arrayValue
                                    }
                                }
                            }
                        }
                        
                        compltion(answers, nil)
                        return
                    }
                    
                }
                
            }
            
        }
    }

    // Get Medical Rep Data
    
    class func GetMedicalRepData(api_token : String!,compltion : @escaping (_ error:Error?,_ LastLogin: String? , _ Name : String? , _ Photo : String?)->Void) {
        print("My Test" , api_token)
        let url = URL(string: UserDataUrl)!
        let parameters = [
          
            "api_token" : api_token
            ] as [String : Any]
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
                
                
            case .failure(let error) :
                print(error)
                compltion(error, nil, nil, nil)
                return
            case .success(let value):
                let json = JSON(value)
                print(json)
                let name = json["name"].string
                let last_login = json["last_login"].string
                let photo = json["photo"].string
                compltion(nil, last_login, name, photo)
                return
            }
        }
        
    }
    
    // Get All Countries
    class func GetCountrie(compltion : @escaping (_ error:Error?,_ CountryData: [String]?)->Void) {
        
        var MyCountry = [String]()
        let url = URL(string: CountriesUrl)!
       
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error, nil)
                return
            case .success(let value ) :
                let json = JSON(value)
                if let data  = json["data"].array {
                    for obj in data {
                        if let obj = obj["name"].string {
                            MyCountry.append(obj)
                        }
                    }
                }
                print(MyCountry)
                compltion(nil, MyCountry)
                return
            }
        }
    }
    
    
    // Get Country Cities
    
    class func GetCities(Name : String!,compltion : @escaping (_ error:Error?,_ CitiesData: [String]?)->Void) {
            let parameters = [
                "name" : Name
        ] as [String : Any]
        let url = URL(string: CitiesUrl)!
        var MyCities = [String]()
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error, nil)
                return
            case .success(let value) :
                let json = JSON(value)
                if let data = json["data"].array {
                    for obj in data {
                        if let obj = obj["name"].string {
                            MyCities.append(obj)
                        }
                    }
                }
                compltion(nil, MyCities)
                return
            }
        }
    }
    
    class func GetModerate(api_token : String! , HcpId : String! , questionId : String!,compltion : @escaping (_ error:Error?,_ status: Bool? , _ mild : String? , _ moderate : String?)->Void) {
        let parameters = [
            "doctor_id" : HcpId ,
            "api_token" : api_token ,
            "question_id" : questionId
            ] as [String : Any]
        let url = URL(string: GetModerateUrl)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error, nil , nil , nil )
                return
            case .success(let value) :
                var mildValue : String!
                var ModerValue : String!
                let json = JSON(value)
                print("Moderate" , json)

                if let data = json["data"].dictionary {
                  //  print("Moderate" , data)
                    if let mild  = data["mild"]?.string {
                        mildValue = mild
                    }
                    if let mod = data["moderate"]?.string {
                        ModerValue = mod
                    }
                }
                compltion(nil, true , mildValue , ModerValue)
                return
            }
        }
    }
    class func AddModerate(api_token : String! , HcpId : String! , questionId : String! , mild : String! , mold : String!) {
        
        let parameters = [
            "doctor_id" : HcpId ,
            "api_token" : api_token ,
            "question_id" : questionId ,
            "mild" : mild ,
            "moderate" : mold
            ] as [String : Any]
        let url = URL(string: AddModUrl)!
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
            case .success(let value) :
               let json = JSON(value)
               print("Moderate Json" , json)
                return
            }
        }
    }
    
}


















