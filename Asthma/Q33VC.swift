//
//  Q33VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q33VC: UIViewController {
    
    var QuestionAnswer33 : String!
    var QuestionThirtyThreeArray = [String]()
    
    @IBOutlet weak var ButtonOne: UIButton!  // 159
    
    @IBOutlet weak var ButtonTwo: UIButton!  //160
    
    @IBOutlet weak var ButtonThree: UIButton!  //161
    
    @IBOutlet weak var ButtonFour: UIButton!  //162
    
    @IBOutlet weak var ButtonFive: UIButton!  // 163
    
    @IBOutlet weak var ButtonSex: UIButton!  //164
    
    
    @IBOutlet weak var ButtonSeven: UIButton!  //223
    
    @IBOutlet weak var ButtonEight: UIButton!
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true

        ApiMethods.GetQuestionAnswer(Question_id: "36"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionThirtyThreeArray.append(obj!)

                    let FirstValue = "159"
                    let SeconValue = "160"
                    let ThirdValue = "161"
                    let FourthValue = "162"
                    let FifthValue = "163"
                    let SexValue = "164"
                    let SeventhValue = "223"
                    let EightValue = "224"
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SeventhValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SeventhValue)
                     }
                     // Button Eight
                     if  obj == EightValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: EightValue)
                     }
                   
                }
            }
            print("MyTest Array :-" , self.QuestionThirtyThreeArray)
            
        }
        
        
    }
    
    @IBAction func NumberOne(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "159")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyThreeArray, Value: "159") { MyArray in
            
            self.QuestionThirtyThreeArray = MyArray
            
        }
    }
    
    @IBAction func NumberTwo(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "160")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyThreeArray, Value: "160") { MyArray in
            
            self.QuestionThirtyThreeArray = MyArray
            
        }
    }
    
    @IBAction func NumberThree(_ sender: DLRadioButton) {
        
        QuestionAnswer(Button: sender, Value: "161")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyThreeArray, Value: "161") { MyArray in
            
            self.QuestionThirtyThreeArray = MyArray
            
        }
    }
    
    @IBAction func NumberFour(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "162")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyThreeArray, Value: "162") { MyArray in
            
            self.QuestionThirtyThreeArray = MyArray
            
        }

    }
    
    
    @IBAction func NumberFive(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "163")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyThreeArray, Value: "163") { MyArray in
            
            self.QuestionThirtyThreeArray = MyArray
            
        }
    }
    
    @IBAction func NumberSex(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "164")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyThreeArray, Value: "164") { MyArray in
            
            self.QuestionThirtyThreeArray = MyArray
            
        }
    }
    
    @IBAction func NumberSeven(_ sender: UIButton) {
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyThreeArray, Value: "223") { MyArray in
            
            self.QuestionThirtyThreeArray = MyArray
            
        }
    }
    
    @IBAction func NumberEight(_ sender: UIButton) {
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)
        
        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyThreeArray, Value: "224") { MyArray in
            
            self.QuestionThirtyThreeArray = MyArray
            
        }
    }
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionThirtyThreeAnswer", Value: [])
        
        ApiToken.SetItems(Key: "QuestionThirtyThreeComment", Value: "")
    }
    
    @IBAction func NextButtonAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirtyThreeAnswer", Value: self.QuestionThirtyThreeArray)
        ApiToken.SetItems(Key: "QuestionThirtyThreeComment", Value: self.QuestionComment.text)
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirtyThreeAnswer", Value: self.QuestionThirtyThreeArray)
        ApiToken.SetItems(Key: "QuestionThirtyThreeComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer33 = Value
        self.NextButton.isHidden = false

        
    }
    
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
}




