//
//  RemindModel.swift
//  Asthma
//
//  Created by MOHAMED on 7/9/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import Foundation
import SwiftyJSON

class ReminderModel {
    var id : String!
    var start : String!
    
    init(HCPDict : [String:JSON]) {
        
        if let ID = HCPDict["id"]?.int {
            self.id = String(ID)
            print("ID :- " , ID)
        }
        
        if let Start = HCPDict["start"]?.string {
            self.start = Start
        }
        
        
    }
    
}

