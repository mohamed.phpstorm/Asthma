//
//  QFinalVC.swift
//  Asthma
//
//  Created by MOHAMED on 7/9/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class QFinalVC: UIViewController {
    var pdflink : String!
    
    
    @IBOutlet weak var UserImage: UIButton!
    
    @IBOutlet weak var UserNametxt: UILabel!
    
    @IBOutlet weak var LastLogintxt: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.GetLink()
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.GetMedicalRepData(api_token: api_token){ (error , last_login , name , photo) in
                if error == nil {
                    self.LastLogintxt.text = last_login
                    self.UserNametxt.text = name
                    if photo != nil {
                         let ImageUrl = URL(string: ImageRoot + photo!)!
                            self.UserImage.kf.setImage(with: ImageUrl, for: .normal)
                        } else {
                            self.UserImage.setImage(#imageLiteral(resourceName: "Avatar"), for: .normal)
                        }
               
                }
            }
        }
    }
    
     func GetLink() {
        if let hcp_Id = ApiToken.GetItem(Key: "Hcp_Id"){
            if let api_token = ApiToken.getApiToken() {
                let url = URL(string: GetPdfUrl)!
                
                let parameters = [
                    
                    "api_token" : api_token,
                    "doctor_id" : hcp_Id
                    ] as [String : Any]
                
                
                Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                    switch response.result {
                    case .failure(let error) :
                        print(error)
                    case.success(let value) :
                        print(value)
                        let json = JSON(value)
                        let data = json["data"].dictionary
                        let pdf = data?["result_pdf"]?.string
                        self.pdflink = pdf
                        print(self.pdflink)
                    }
                }
                
            }
        }
    }
    func AlertAction(message : String, title : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func QReportAction(_ sender: UIButton) {
        if pdflink != nil {
            if let api_token = ApiToken.getApiToken() {
            if let url = URL(string: MainUrl + pdflink + "?api_token=" + api_token){
                UIApplication.shared.openURL(url)
            }
            }
        }else {
            let title = ""
            let message = "Please Wait while retrieving your data"
            self.AlertAction(message: title, title: message)
        }
      
        
    }
    
    
    @IBAction func QJourneyAction(_ sender: UIButton) {
        
        performSegue(withIdentifier: "PatientJourneySegue", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dist = segue.destination as? ReportVC {
            if let hcp_Id = ApiToken.GetItem(Key: "Hcp_Id"){
                dist.Hcp_Id = hcp_Id
            }
            
        }
        
    }
    
    @IBAction func MenuBuAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuVC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    @IBAction func NotificationAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SchedualScreen") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func LogoutAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogoutScreen") as! LogoutVC
        self.present(vc, animated: true, completion: nil)
    }
    
    

    
}


