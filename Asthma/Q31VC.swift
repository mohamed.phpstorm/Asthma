//
//  Q31VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
class Q31VC: UIViewController {

    
    var QuestionAnswer31 : String!
    
    var QuestionThirtyOneArray = [String]()
    
    
    @IBOutlet weak var ButtonOne: UIButton!  //147
    
    @IBOutlet weak var ButtonTwo: UIButton!  //148
    
    @IBOutlet weak var ButtonThree: UIButton!  //149
    
    @IBOutlet weak var ButtonFour: UIButton!   //150
    
    @IBOutlet weak var ButtonFive: UIButton!  //151
    
    
    @IBOutlet weak var ButtonSex: UIButton!   //152
    
    
    
    
    
    
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
        ApiMethods.GetQuestionAnswer(Question_id: "34"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    let FirstValue = "147"
                    let SeconValue = "148"
                    let ThirdValue = "149"
                    let FourthValue = "150"
                    let FifthValue = "151"
                    let SexValue = "152"
                    var obj = obj["answer_id"].string
                    self.QuestionThirtyOneArray.append(obj!)

                    
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }/*
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionThirtyOneArray)
            
        }
        
        
    }
    
    @IBAction func NumberOne(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "147")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyOneArray, Value: "147") { MyArray in
            
            self.QuestionThirtyOneArray = MyArray
            
        }
    }
    
    
    @IBAction func NumberTwo(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "148")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyOneArray, Value: "148") { MyArray in
            
            self.QuestionThirtyOneArray = MyArray
            
        }


    }
    
    
    @IBAction func NumberThree(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "149")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyOneArray, Value: "149") { MyArray in
            
            self.QuestionThirtyOneArray = MyArray
            
        }


    }
    
    
    @IBAction func NumberFour(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "150")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyOneArray, Value: "150") { MyArray in
            
            self.QuestionThirtyOneArray = MyArray
            
        }


    }
    
    
    @IBAction func NumberFive(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "151")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyOneArray, Value: "151") { MyArray in
            
            self.QuestionThirtyOneArray = MyArray
            
        }

    }
    
    @IBAction func NumberSex(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "152")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionThirtyOneArray, Value: "152") { MyArray in
            
            self.QuestionThirtyOneArray = MyArray
            
        }

    }
    
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionThirtyOneAnswer", Value: [])
        
        ApiToken.SetItems(Key: "QuestionThirtyOneComment", Value: "")
    }
    @IBAction func NextButton(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirtyOneAnswer", Value: self.QuestionThirtyOneArray)
        ApiToken.SetItems(Key: "QuestionThirtyOneComment", Value: self.QuestionComment.text)
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        ApiToken.SetArray(Key: "QuestionThirtyOneAnswer", Value: self.QuestionThirtyOneArray)
        ApiToken.SetItems(Key: "QuestionThirtyOneComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer31 = Value
        self.NextButton.isHidden = false
    }
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
}




