//
//  Q29VC.swift
//  Asthma
//
//  Created by MOHAMED on 6/18/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
class Q29VC: UIViewController {

    var QuestionAnswer29 : String!

    var QuestionTwentyNineArray = [String]()
    
    
    @IBOutlet weak var ButtonOne: UIButton!  //138
    
    @IBOutlet weak var ButtonTwo: UIButton!  //139
    
    @IBOutlet weak var ButtonThree: UIButton!  //140
    
    @IBOutlet weak var ButtonFour: UIButton!  //141
    
    
    
    
    
    @IBOutlet weak var NextButton: UIButton!
    @IBOutlet weak var QuestionComment: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.NextButton.isHidden = true
        ApiMethods.GetQuestionAnswer(Question_id: "32"){(answers , error , comment) in
            
            if answers != nil {
                self.NextButton.isHidden = false
                self.QuestionComment.text = comment

                for obj in answers! {
                    var obj = obj["answer_id"].string
                    self.QuestionTwentyNineArray.append(obj!)

                    let FirstValue = "138"
                    let SeconValue = "139"
                    let ThirdValue = "140"
                    let FourthValue = "141"
            
                    
                    // ButtonOne
                    if  obj == FirstValue as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonOne, Value: FirstValue)
                    }
                    // Button Two
                    if  obj == SeconValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonTwo, Value: SeconValue)
                    }
                    // Button Three
                    if  obj == ThirdValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonThree, Value: ThirdValue)
                    }
                    // Button Four
                    if  obj == FourthValue  as? String {
                        ApiToken.CheckQuestionValue(Button: self.ButtonFour, Value: FourthValue)
                    }
                    /*
                     // Button Five
                     if  obj == FifthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonFive, Value: FifthValue)
                     }
                     // Button Sex
                     if  obj == SexValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSex, Value: SexValue)
                     }
                     // Button Seven
                     if  obj == SevnthValue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonSeven, Value: SevnthValue)
                     }
                     // Button Eight
                     if  obj == Eightvalue  as? String {
                     ApiToken.CheckQuestionValue(Button: self.ButtonEight, Value: Eightvalue)
                     }
                     */
                }
            }
            print("MyTest Array :-" , self.QuestionTwentyNineArray)
            
        }
        
        
    }
    
    
    
    @IBAction func NumberOne(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "138")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentyNineArray, Value: "138") { MyArray in
            
            self.QuestionTwentyNineArray = MyArray
            
        }
    }
    
    
    @IBAction func NumberTwo(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "139")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentyNineArray, Value: "139") { MyArray in
            
            self.QuestionTwentyNineArray = MyArray
            
        }
    }
    
    @IBAction func NumberThree(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "140")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)

        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentyNineArray, Value: "140") { MyArray in
            
            self.QuestionTwentyNineArray = MyArray
            
        }
    }
    
    
    
    @IBAction func NumberFour(_ sender: DLRadioButton) {
        QuestionAnswer(Button: sender, Value: "141")
        ApiToken.ChangeCheckBoxImage(CheckBox: sender)


        ApiToken.SetArrayValue(Sender: sender, Questiontest: QuestionTwentyNineArray, Value: "141") { MyArray in
            
            self.QuestionTwentyNineArray = MyArray
            
        }
    }
    
    
    @IBAction func SkipActionBu(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionTwentyNineAnswer", Value: [])
        
        ApiToken.SetItems(Key: "QuestionTwentyNineComment", Value: "")
    }
    
    
    @IBAction func NextButtonAction(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionTwentyNineAnswer", Value: self.QuestionTwentyNineArray)
        ApiToken.SetItems(Key: "QuestionTwentyNineComment", Value: self.QuestionComment.text)
    }
    
    // Save Questionier To This Question
    @IBAction func SaveAction(_ sender: UIButton) {
        
        ApiToken.SetArray(Key: "QuestionTwentyNineAnswer", Value: self.QuestionTwentyNineArray)
        ApiToken.SetItems(Key: "QuestionTwentyNineComment", Value: self.QuestionComment.text)
        if let api_token = ApiToken.getApiToken() {
            ApiMethods.AddQuestionier(api_token: api_token) {(error , status) in
                if error != nil {
                    ApiToken.AlertAction(message: "Please check your internet connection ", title: "error", VC: self)
                    
                }else {
                    RemoveData.removeDataObj()
                }
                
                
            }
            
        }
    }
    
    
    func QuestionAnswer(Button : DLRadioButton , Value : String)  {
        self.QuestionAnswer29 = Value
        self.NextButton.isHidden = false
  
    }
    
    
    @IBAction func Presentation(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentationViewController") as! Q1VC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func Diagnostic(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DiagnosticViewController") as! Q12VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func Management(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ManagementViewController") as! Q17VC
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func FollowUp(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FollowUpViewController") as! Q30VC
        self.present(vc, animated: true, completion: nil)
        
    }
}




